package view.components;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.RefineryUtilities;

/**
 * classe per il grafico a torta
 */
public class AWTPieChart extends JFrame {
	
	private static final long serialVersionUID = -1753760388662708023L;
	
	private static Map<String,Integer> data = new HashMap<>();
	
	/**
	 * costruttore del grafico
	 * @param title titolo grafico
	 * @param data mappa contenente i dati
	 */
	public AWTPieChart(String title, Map<String,Integer> data) {
		super( title ); 
	    AWTPieChart.data = data;
	    setContentPane(createDemoPanel());
	    this.setSize(600, 600);
	    RefineryUtilities.centerFrameOnScreen(this);
	    this.setVisible(true);
	}
	
	/**
	 * metodo che crea il dataset
	 * @return il dataset contenente i dati
	 */
	private static PieDataset createDataset(){
		DefaultPieDataset dataset = new DefaultPieDataset( );
	    for(Map.Entry<String, Integer> e: data.entrySet()){
	    	dataset.setValue(String.valueOf(e.getKey()), e.getValue());
		}
	    return dataset;         
	}
	  
	/**
	 * metodo che crea il grafico
	 * @param dataset dataset con i dati
	 * @return il grafico
	 */
	private static JFreeChart createChart(PieDataset dataset){

		return ChartFactory.createPieChart(
          "Percentuale crimini commessi dai reclusi attuali",  // chart title
           dataset,        // data
           true,           // include legend
           true,
           false);
	}
	  
	private static JPanel createDemoPanel(){
		JFreeChart chart = createChart(createDataset( ) );  
	    return new ChartPanel( chart ); 
	}

}
