package view.components;
import java.util.Map;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel; 
import org.jfree.chart.JFreeChart; 
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset; 
import org.jfree.data.category.DefaultCategoryDataset; 
import org.jfree.ui.RefineryUtilities; 

/**
 * classe per il bar chart
 */
public class AWTBarChart extends JFrame{
	
	private static final long serialVersionUID = -7145050658657438095L;
	
	private final Map<Integer,Integer> data;
	
	/**
	 * costruttore bar chart
	 * @param data mappa da cui prendere i dati
	 * @param title titolo grafico
	 * @param chartName nome grafico
	 */
	public AWTBarChart(Map<Integer, Integer> data, String title, String chartName) {
		
		super(title);
		this.data = data;
		JFreeChart barChart = ChartFactory.createBarChart(
		         chartName,           
		         "Anno",            
		         "Numero di prigonieri",            
		         createDataset(),          
		         PlotOrientation.VERTICAL,           
		         true, true, false);
	   ChartPanel chartPanel = new ChartPanel( barChart );        
	   chartPanel.setPreferredSize(new java.awt.Dimension( 560 , 367 ) );        
	   setContentPane( chartPanel ); 
	   this.pack( );        
	   RefineryUtilities.centerFrameOnScreen( this );        
	   this.setVisible( true ); 
	   this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	   
	}
	
	/**
	 * metodo che inserisce i dati nel diagramma
	 * @return il dataset
	 */
	private CategoryDataset createDataset( ){
		final String name = "-";
	    final DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
	    for(Map.Entry<Integer, Integer> e: data.entrySet()){
	    	dataset.addValue(Double.valueOf(e.getValue()), e.getKey(), name);
	    }
	    return dataset; 
	}
	
}