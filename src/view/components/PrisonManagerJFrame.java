package view.components;

import javax.swing.*;
import java.awt.*;

import static controller.templates.BaseController.getResourcesFolderPath;

/**
 * jframe predefinito per il programma
 */
public class PrisonManagerJFrame extends JFrame{

	private static final long serialVersionUID = 4583640093618196192L;

	/**logo programma*/
    private final String logoPath = getResourcesFolderPath() + "logo.png";
	private final ImageIcon img = new ImageIcon(logoPath);

    protected final view.components.PrisonManagerJPanel center;
	
	/**costruttore jframe*/
	protected PrisonManagerJFrame(){
		this.setIconImage(img.getImage());
		setResizable(false);
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        center = new view.components.PrisonManagerJPanel(new FlowLayout());
	}

	public void createTable(JTable table){
        table.setPreferredScrollableViewportSize(new Dimension(500,300));
        table.setFillsViewportHeight(true);
        JScrollPane js=new JScrollPane(table);
        js.setVisible(true);
        center.add(js);
    }
}
