package view.utils;

/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 


import javax.swing.*;
import java.awt.*;
import java.util.Objects;

/**
 * A 1.4 file that provides utility methods for
 * creating form- or grid-style layouts with SpringLayout.
 * These utilities are used by several programs, such as
 * SpringBox and SpringCompactGrid.
 */
public class SpringUtilities {

    /**
     * Aligns the first <code>rows</code> * <code>cols</code>
     * components of <code>parent</code> in
     * a grid. Each component in a column is as wide as the maximum
     * preferred width of the components in that column;
     * height is similarly determined for each row.
     * The parent is made just big enough to fit them all.
     *
     * @param rowsNumber number of rows
     * @param columnsNumber number of columns
     * @param initialX x location to start the grid at
     * @param initialY y location to start the grid at
     * @param xPadding x padding between cells
     * @param yPadding y padding between cells
     */
    public static void makeCompactGrid(Container parent,
                                       int rowsNumber, int columnsNumber,
                                       int initialX, int initialY,
                                       int xPadding, int yPadding) {
        SpringLayout layout = getSpringLayout(parent);
        Spring xAxis = alignCellsInColumns(initialX, columnsNumber, rowsNumber, parent, xPadding);
        Spring yAxis = alignCellsInRows(initialY, rowsNumber, columnsNumber, parent, yPadding);
        setParentSize(layout, parent, yAxis, xAxis);
    }


    //Align all cells in each column and make them the same width.
    private static Spring alignCellsInColumns(int initialX, int columnsNumber,
                                              int rowsNumber, Container parent, int xPadding){
        Spring x = Spring.constant(initialX);
        for (int c = 0; c < columnsNumber; c++) {
            Spring width = calculateWidthSpring(columnsNumber, rowsNumber, parent, c);
            setWidthConstraints(columnsNumber, rowsNumber, parent, x, c, width);
            x = Spring.sum(x, Spring.sum(width, Spring.constant(xPadding)));
        }
        return x;
    }

    private static void setWidthConstraints(int columnsNumber, int rowsNumber, Container parent, Spring x, int c, Spring width) {
        for (int r = 0; r < rowsNumber; r++) {
            SpringLayout.Constraints constraints =
                    getConstraintsForCell(r, c, parent, columnsNumber);
            constraints.setX(x);
            constraints.setWidth(width);
        }
    }

    private static Spring calculateWidthSpring(int columnsNumber, int rowsNumber, Container parent, int c) {
        Spring width = Spring.constant(0);
        for (int r = 0; r < rowsNumber; r++) {
            width = Spring.max(width,
                    getConstraintsForCell(r, c, parent, columnsNumber).
                            getWidth());
        }
        return width;
    }


    //Align all cells in each row and make them the same height.
    private static Spring alignCellsInRows(int initialY, int rowsNumber,
                                           int columnsNumber, Container parent, int yPadding){
        Spring y = Spring.constant(initialY);
        for (int r = 0; r < rowsNumber; r++) {
            Spring height = calculateHeightSpring(columnsNumber, parent, r);
            setHeightConstraints(columnsNumber, parent, y, r, height);
            y = Spring.sum(y, Spring.sum(height, Spring.constant(yPadding)));
        }
        return y;
    }

    private static void setHeightConstraints(int columnsNumber, Container parent, Spring y, int r, Spring height) {
        for (int c = 0; c < columnsNumber; c++) {
            SpringLayout.Constraints constraints =
                    getConstraintsForCell(r, c, parent, columnsNumber);
            constraints.setY(y);
            constraints.setHeight(height);
        }
    }

    private static Spring calculateHeightSpring(int columnsNumber, Container parent, int r) {
        Spring height = Spring.constant(0);
        for (int c = 0; c < columnsNumber; c++) {
            height = Spring.max(height,
                    getConstraintsForCell(r, c, parent, columnsNumber).
                            getHeight());
        }
        return height;
    }


    private static SpringLayout getSpringLayout(Container parent){
        try {
            return (SpringLayout) parent.getLayout();
        } catch (ClassCastException exception) {
            System.err.println("The first argument to makeCompactGrid must use SpringLayout.");
            return null;
        }
    }

    private static SpringLayout.Constraints getConstraintsForCell(
            int row, int col,
            Container parent,
            int cols) {
        SpringLayout layout = (SpringLayout) parent.getLayout();
        Component c = parent.getComponent(row * cols + col);
        return layout.getConstraints(c);
    }


    private static void setParentSize(SpringLayout layout, Container parent, Spring yAxis, Spring xAxis){
        SpringLayout.Constraints pCons = Objects.requireNonNull(layout).getConstraints(parent);
        pCons.setConstraint(SpringLayout.SOUTH, yAxis);
        pCons.setConstraint(SpringLayout.EAST, xAxis);
    }
}