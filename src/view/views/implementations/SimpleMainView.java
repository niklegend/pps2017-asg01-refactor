package view.views.implementations;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;

import controller.Implementations.MainController.InsertPrisonerListener;
import controller.Implementations.MainController.LogoutListener;
import controller.Implementations.MainController.MoreFunctionsListener;
import controller.Implementations.MainController.RemovePrisonerListener;
import controller.Implementations.MainController.SupervisorListener;
import controller.Implementations.MainController.ViewPrisonerListener;
import view.components.PrisonManagerJFrame;
import view.components.PrisonManagerJPanel;

public class SimpleMainView extends PrisonManagerJFrame implements view.views.interfaces.MainView {

	private static final long serialVersionUID = -2585136897389059255L;
	private static final int FRAME_WIDTH = 550;
	private static final int FRAME_HEIGHT = 150;

	private PrisonManagerJPanel center;
	private final JButton addPrisoner= new JButton("Aggiungi prigioniero");
	private final JButton removePrisoner=new JButton("Rimuovi prigioniero");
    private final JButton viewPrisoner=new JButton("Vedi profilo prigioniero");
	private PrisonManagerJPanel south;
	private final JButton highRankOnly = new JButton("Funzioni riservate (Grado 3)");
	private final JButton moreFunctions = new JButton("Altre funzioni (grado 2)");
	private final JButton logout=new JButton("Logout");
	private PrisonManagerJPanel north;
	private final JLabel title = new JLabel("Prison Manager");
	private final int rank;
	
	/**
	 * costruttore
	 * @param rank il rank della guardia che sta visualizzando il programma
	 */
	public SimpleMainView(int rank){
		this.rank=rank;
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		buildCenterPanel();
		buildSouthPanel(rank);
		buildNorthPanel();
		this.setVisible(true);
	}

	private void buildNorthPanel() {
		north = new PrisonManagerJPanel(new FlowLayout());
		north.add(title);
		this.getContentPane().add(BorderLayout.NORTH,north);
	}

	private void buildSouthPanel(int rank) {
		south=new PrisonManagerJPanel(new FlowLayout());
		south.add(moreFunctions);
		south.add(highRankOnly);
		south.add(logout);
		if(rank < 2){
			moreFunctions.setEnabled(false);
		}
		if(rank < 3){
			highRankOnly.setEnabled(false);
		}
		this.getContentPane().add(BorderLayout.SOUTH,south);
	}

	private void buildCenterPanel() {
		center = new PrisonManagerJPanel(new FlowLayout());
		center.add(addPrisoner);
		center.add(removePrisoner);
		center.add(viewPrisoner);
		this.getContentPane().add(BorderLayout.CENTER,center);
	}

	public void addLogoutListener(LogoutListener logoutListener){
		logout.addActionListener(logoutListener);
	}
	
	public void addInsertPrisonerListener(InsertPrisonerListener insertPrisonerListener){
		addPrisoner.addActionListener(insertPrisonerListener);
	}
	
	public void addRemovePrisonerListener(RemovePrisonerListener removePrisonerListener){
		removePrisoner.addActionListener(removePrisonerListener);
	}
	
	public void addViewPrisonerListener(ViewPrisonerListener viewPrisonerListener){
		viewPrisoner.addActionListener(viewPrisonerListener);
	}
	
	public void addMoreFunctionsListener(MoreFunctionsListener moreFListener){
		moreFunctions.addActionListener(moreFListener);
	}
	
	public void addSupervisorListener(SupervisorListener supervisorListener){
		highRankOnly.addActionListener(supervisorListener);
	}
	
	public int getRank(){
		return this.rank;
	}
}
