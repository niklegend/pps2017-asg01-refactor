package view.views.implementations;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.InsertGuardController.BackListener;
import controller.Implementations.InsertGuardController.InsertListener;
import model.builders.SimpleGuardBuilder;
import model.Interfaces.Guard;
import view.components.PrisonManagerJFrame;
import view.components.PrisonManagerJPanel;
import view.utils.SpringUtilities;

public class SimpleInsertGuardView extends PrisonManagerJFrame implements view.views.interfaces.InsertGuardView {

    private static final long serialVersionUID = 6919464397187101572L;

	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 400;
	private static final int ROWS_NUMBER = 7;
	private static final int COLUMNS_NUMBER = 2;
	private static final int INITIAL_X = 6;
	private static final int INITIAL_Y = 6;
	private static final int X_PADDING = 6;
	private static final int Y_PADDING = 6;

    private static final String DEFAULT_GUARD_ID = "0";
    private static final String DEFAULT_BIRTH_DATE = "01/01/1980";
    private static final String DEFAULT_RANK = "0";

    private final String pattern = "MM/dd/yyyy";
    private final SimpleDateFormat format = new SimpleDateFormat(pattern);
    private final int rank;

	private PrisonManagerJPanel south;
	private final JButton insert = new JButton("Inserisci");
	private PrisonManagerJPanel north;
	private final JLabel guardID = new JLabel("ID Guardia");
	private final JTextField guardIDField = new JTextField(6);
	private final JLabel name = new JLabel("Nome");
	private final JTextField nameField = new JTextField(6);
	private final JLabel surname = new JLabel("Cognome");
	private final JTextField surnameField = new JTextField(6);
	private final JLabel birthDate = new JLabel("Data di nascita (mm/gg/aaaa)");
	private final JTextField birthDateField = new JTextField(6);
	private PrisonManagerJPanel center;
	private final JLabel guardRank = new JLabel("Grado (1-2-3)");
	private final JTextField guardRankField = new JTextField(8);
	private final JLabel telephoneNumber = new JLabel("Numero di telefono");
	private final JTextField telephoneNumField = new JTextField(8);
	private final JLabel password = new JLabel("Password(6 caratt. min)");
	private final JTextField passwordField = new JTextField(8);
	private final JButton back = new JButton("Indietro");
	private final JLabel title = new JLabel("Inserisci una guardia");

	
	/**
	 * costruttore
	 * @param rank il rank della guardia che sta visualizzando il programma
	 */
	public SimpleInsertGuardView(int rank){
		this.rank=rank;
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
        buildNorthPanel();
        buildCenterPanel();
        buildSouthPanel();
		this.setVisible(true);
	}

    private void buildSouthPanel() {
        south = new PrisonManagerJPanel(new FlowLayout());
        south.add(insert);
        south.add(back);
        this.getContentPane().add(BorderLayout.SOUTH,south);
    }

    private void buildCenterPanel() {
        center = new PrisonManagerJPanel(new SpringLayout());
		insertGuardField();
		insertNameLabel();
		insertSurnameLabel();
		insertBirthDateLabel();
		insertTelephoneLabel();
		insertRankField();
		insertPasswordLabel();
        SpringUtilities.makeCompactGrid(center,
                ROWS_NUMBER, COLUMNS_NUMBER, //rows, cols
                INITIAL_X, INITIAL_Y,        //initX, initY
                X_PADDING, Y_PADDING);       //xPad, yPad
        this.getContentPane().add(BorderLayout.CENTER,center);
    }

	private void insertPasswordLabel() {
		center.add(password);
		center.add(passwordField);
	}

	private void insertRankField() {
		center.add(guardRank);
		center.add(guardRankField);
		guardRankField.setText(DEFAULT_RANK);
	}

	private void insertTelephoneLabel() {
		center.add(telephoneNumber);
		center.add(telephoneNumField);
	}

	private void insertBirthDateLabel() {
		center.add(birthDate);
		center.add(birthDateField);
		birthDateField.setText(DEFAULT_BIRTH_DATE);
	}

	private void insertSurnameLabel() {
		center.add(surname);
		center.add(surnameField);
	}

	private void insertNameLabel() {
		center.add(name);
		center.add(nameField);
	}

	private void insertGuardField() {
		center.add(guardID);
		center.add(guardIDField);
		guardIDField.setText(DEFAULT_GUARD_ID);
	}

	private void buildNorthPanel() {
        north = new PrisonManagerJPanel(new FlowLayout());
        north.add(title);
        this.getContentPane().add(BorderLayout.NORTH,north);
    }

    public void addBackListener(BackListener backListener){
		back.addActionListener(backListener);
	}
	
	public void displayErrorMessage(String error){
		JOptionPane.showMessageDialog(this, error);
	}
	
	public void addInsertListener(InsertListener insertListener){
		insert.addActionListener(insertListener);
	}

	public int getRank() {
		return this.rank;
	}
	
	public Guard getGuard(){
		Date birth = null;
		try {
			birth = format.parse(birthDateField.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new SimpleGuardBuilder().setName(nameField.getText()).setSurname(surnameField.getText()).setBirthDate(birth).setRank(Integer.valueOf(guardRankField.getText())).setTelephoneNumber(telephoneNumField.getText()).setGuardID(Integer.valueOf(guardIDField.getText())).setPassword(passwordField.getText()).createSimpleGuard();
	}

}
