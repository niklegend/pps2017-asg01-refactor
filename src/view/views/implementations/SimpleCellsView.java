package view.views.implementations;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;

import controller.Implementations.ViewCellsController.BackListener;
import view.components.PrisonManagerJFrame;
import view.components.PrisonManagerJPanel;
import view.views.interfaces.CellsView;

public class SimpleCellsView extends PrisonManagerJFrame implements CellsView {

    private static final long serialVersionUID = -4382628436360052382L;

    private static final int FRAME_WIDTH = 550;
    private static final int FRAME_HEIGHT = 430;

    private PrisonManagerJPanel south;
    private PrisonManagerJPanel north;

    private final JButton back = new JButton("Indietro");
    private final JLabel title = new JLabel("Vedi celle : ");

    private final int rank;

	/**
     * costruttore
     * @param rank il rank della guardia che sta visualizzando il programma
     */
	public SimpleCellsView(int rank){
		this.rank=rank;
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		buildNorthPanel();
        buildCenterPanel();
        buildSouthPanel();
		this.setVisible(true);
	}

    private void buildSouthPanel() {
        south = new PrisonManagerJPanel(new FlowLayout());
        south.add(back);
        this.getContentPane().add(BorderLayout.SOUTH,south);
    }

    private void buildCenterPanel() {
        this.getContentPane().add(BorderLayout.CENTER,center);
    }

    private void buildNorthPanel(){
        north = new PrisonManagerJPanel(new FlowLayout());
        north.add(title);
        this.getContentPane().add(BorderLayout.NORTH,north);
    }

	 public int getRank(){
	    	return this.rank;
	    }
	 
	 public void addBackListener(BackListener backListener){
			back.addActionListener(backListener);
		}

}
