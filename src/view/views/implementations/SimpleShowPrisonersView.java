package view.views.implementations;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import controller.Implementations.ShowPrisonersController.BackListener;
import controller.Implementations.ShowPrisonersController.ComputeListener;
import view.components.PrisonManagerJFrame;
import view.components.PrisonManagerJPanel;

public class SimpleShowPrisonersView extends PrisonManagerJFrame implements view.views.interfaces.ShowPrisonersView {

    private static final long serialVersionUID = 7096756054745433188L;

    private static final int FRAME_WIDTH = 800;
    private static final int FRAME_HEIGHT = 370;
    private static final String DEFAULT_IMPRISONMENT_START_DATE = "01/31/1980";
    private static final String DEFAULT_IMPRISONMENT_END_DATE = "01/31/2021";
    private static final int START_DATE_FIELD_COLUMNS = 6;
    private static final int END_DATE_FIELD_COLUMNS = 6;

    private PrisonManagerJPanel north;
    private PrisonManagerJPanel center;
    private PrisonManagerJPanel south;

	private final JLabel from=new JLabel("Reclusi Da (mm/gg/aaaa)");
	private final JLabel to=new JLabel("A (mm/gg/aaaa)");
	private final JTextField imprisonmentStartDateField = new JTextField(START_DATE_FIELD_COLUMNS);
	private final JTextField imprisonmentEndDateField = new JTextField(END_DATE_FIELD_COLUMNS);

    private JTable table = new JTable();
	private final JButton compute = new JButton("Calcola");
	private final JButton back = new JButton("Indietro");
    private JScrollPane scrollPane;

	private final String datePattern = "MM/dd/yyyy";
    private final SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);

    private final int rank;
	
    /**
     * costruttore
     * @param rank il rank della guardia che sta visualizzando il programma
     */
	public SimpleShowPrisonersView(int rank){
		this.rank=rank;
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
        buildNorthPanel();
        buildCenterPanel();
        buildSouthPanel();
		this.setVisible(true);
	}

    private void buildSouthPanel() {
        south = new PrisonManagerJPanel(new FlowLayout());
        south.add(compute);
        south.add(back);
        this.getContentPane().add(BorderLayout.SOUTH,south);
    }

    private void buildCenterPanel() {
        center = new PrisonManagerJPanel(new FlowLayout());
        scrollPane =new JScrollPane(table);
        this.getContentPane().add(BorderLayout.CENTER,center);
    }

    private void buildNorthPanel() {
        north = new PrisonManagerJPanel(new FlowLayout());
        north.add(from);
        north.add(imprisonmentStartDateField);
        imprisonmentStartDateField.setText(DEFAULT_IMPRISONMENT_START_DATE);
        north.add(to);
        north.add(imprisonmentEndDateField);
        imprisonmentEndDateField.setText(DEFAULT_IMPRISONMENT_END_DATE);
        this.getContentPane().add(BorderLayout.NORTH,north);
    }

    public void createTable(JTable table){
    	center.removeAll();
    	this.table=table;
    	table.setPreferredScrollableViewportSize(new Dimension(700,200));
        scrollPane =new JScrollPane(table);
    	center.add(scrollPane);
    	scrollPane.revalidate();
    	this.setVisible(true);
    }

	public int getRank() {
		return rank;
	}
	
	public void addBackListener(BackListener backListener){
		back.addActionListener(backListener);
	}
	
	public void addComputeListener(ComputeListener computeListener){
		compute.addActionListener(computeListener);
	}
	
	public Date getFrom(){
		Date date = null;
		try {
			date = dateFormat.parse(imprisonmentStartDateField.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public Date getTo(){
		Date date = null;
		try {
			date = dateFormat.parse(imprisonmentEndDateField.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
}
