package view.views.implementations;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.Implementations.RemoveGuardController.BackListener;
import controller.Implementations.RemoveGuardController.RemoveGuardListener;
import view.components.PrisonManagerJFrame;
import view.components.PrisonManagerJPanel;

public class SimpleRemoveGuardView extends PrisonManagerJFrame implements view.views.interfaces.RemoveGuardView {

    private static final long serialVersionUID = -4172744505801106816L;

	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 150;
    private static final int GUARD_ID_FIELD_COLUMNS = 6;
    private static final String REMOVE_GUARD_LABEL = "Rimuovi guardia";

    private PrisonManagerJPanel centerPanel;
    private PrisonManagerJPanel southPanel;

	private final JLabel guardID = new JLabel("ID Guardia");
	private final JTextField guardIDField = new JTextField(GUARD_ID_FIELD_COLUMNS);

	private final JButton remove=new JButton("Rimuovi");
	private final JButton back=new JButton("Back");

	private final int rank;
	
	/**
	 * costruttore
	 * @param rank il rank della guardia che sta visualizzando il programma
	 */
	public SimpleRemoveGuardView(int rank){
		this.rank=rank;
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		buildNorthPanel();
		buildCenterPanel();
		buildSouthPanel();
		this.setVisible(true);
	}

	private void buildSouthPanel() {
		southPanel = new PrisonManagerJPanel(new FlowLayout());
		southPanel.add(remove);
		southPanel.add(back);
		this.getContentPane().add(BorderLayout.SOUTH, southPanel);
	}

	private void buildCenterPanel() {
		centerPanel = new PrisonManagerJPanel(new FlowLayout());
		centerPanel.add(guardID);
		centerPanel.add(guardIDField);
		this.getContentPane().add(BorderLayout.CENTER, centerPanel);
	}

	private void buildNorthPanel() {
		PrisonManagerJPanel north = new PrisonManagerJPanel(new FlowLayout());
		JLabel title = new JLabel(REMOVE_GUARD_LABEL);
		north.add(title);
		this.getContentPane().add(BorderLayout.NORTH, north);
	}

	public int getID(){
		if(guardIDField.getText().equals(""))
			return -1;
		return Integer.valueOf(guardIDField.getText());
	}
	
	public void displayErrorMessage(String error){
		JOptionPane.showMessageDialog(this, error);
	}

	public int getRank() {		
		return this.rank;
	}
	
	public void addRemoveGuardListener(RemoveGuardListener removeGuardListener){
		remove.addActionListener(removeGuardListener);
	}

	public void addBackListener(BackListener backListener){
		back.addActionListener(backListener);
	}
}
