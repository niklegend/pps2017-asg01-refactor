package view.views.implementations;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import controller.Implementations.BalanceController.BackListener;
import view.components.PrisonManagerJFrame;
import view.components.PrisonManagerJPanel;

public class SimpleBalanceView extends PrisonManagerJFrame implements view.views.interfaces.BalanceView {

    private static final long serialVersionUID = -9027369697644712989L;

	private static final int FRAME_WIDTH = 600;
    private static final int FRAME_HEIGHT = 400;
    private static final String BALANCE_LABEL = "Bilancio:  ";
    private static final int TABLE_WIDTH = 550;
    private static final int TABLE_HEIGHT = 260;

    private PrisonManagerJPanel center;
    private PrisonManagerJPanel south;
    private PrisonManagerJPanel north;
    private final JButton back = new JButton("Indietro");
    private final JLabel balanceLabel = new JLabel(BALANCE_LABEL);

	private final int rank;
    
    /**
     * costruttore 
     * @param rank il rank della guardia che sta visualizzando il programma
     */
    public SimpleBalanceView(int rank){
    	this.rank=rank;
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		buildNorthPanel();
		buildCenterPanel();
		buildSouthPanel();
		this.setVisible(true);
    }

    private void buildNorthPanel(){
        north = new PrisonManagerJPanel(new FlowLayout());
        north.add(balanceLabel);
        this.getContentPane().add(BorderLayout.NORTH,north);
    }

    private void buildCenterPanel(){
        center = new PrisonManagerJPanel(new FlowLayout());
        this.getContentPane().add(BorderLayout.CENTER,center);
    }

    private void buildSouthPanel(){
        south = new PrisonManagerJPanel(new FlowLayout());
        south.add(back);
        this.getContentPane().add(BorderLayout.SOUTH,south);
    }

    public int getRank(){
    	return this.rank;
    }
    
    public void addBackListener(BackListener backListener){
		back.addActionListener(backListener);
	}
    
    public void setLabel(String balance){
    	balanceLabel.setText(BALANCE_LABEL + balance);
    }
    
    public void createTable(JTable table){
		table.setPreferredScrollableViewportSize(new Dimension(TABLE_WIDTH, TABLE_HEIGHT));
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setVisible(true);
    	center.add(scrollPane);
    }
}
