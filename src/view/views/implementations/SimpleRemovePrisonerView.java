package view.views.implementations;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.Implementations.RemovePrisonerController.BackListener;
import controller.Implementations.RemovePrisonerController.RemoveListener;
import view.components.PrisonManagerJFrame;
import view.components.PrisonManagerJPanel;

public class SimpleRemovePrisonerView extends PrisonManagerJFrame implements view.views.interfaces.RemovePrisonerView {

    private static final long serialVersionUID = 8578870382924181404L;

	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 120;
    private static final int PRISONER_ID_FIELD_COLUMNS = 2;

    private PrisonManagerJPanel northPanel;
	private PrisonManagerJPanel centerPanel;

	private final JButton remove = new JButton("Rimuovi");
	private final JButton back = new JButton("Indietro");

	private final JLabel prisonerID = new JLabel("ID Prigioniero");
	private final JTextField prisonerIDField = new JTextField(PRISONER_ID_FIELD_COLUMNS);

	private final int rank;
	
	/**
	 * costruttore
	 * @param rank il rank della guardia che sta visualizzando il programma
	 */
	public SimpleRemovePrisonerView(int rank){
		this.rank=rank;
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		buildNorthPanel();
		buildCenterPanel();
		this.setVisible(true);
	}

	private void buildCenterPanel() {
		centerPanel = new PrisonManagerJPanel(new FlowLayout());
		centerPanel.add(remove);
		centerPanel.add(back);
		this.getContentPane().add(BorderLayout.CENTER, centerPanel);
	}

	private void buildNorthPanel() {
		northPanel = new PrisonManagerJPanel(new FlowLayout());
		northPanel.add(prisonerID);
		northPanel.add(prisonerIDField);
		this.getContentPane().add(BorderLayout.NORTH, northPanel);
	}

	public void addRemoveListener(RemoveListener removeListener){
		remove.addActionListener(removeListener);
	}

	public void addBackListener(BackListener backListener){
		back.addActionListener(backListener);
	}

	public int getID(){
		if(prisonerIDField.getText().equals(""))
			return -1;
		return Integer.valueOf(prisonerIDField.getText());
	}
	
	public void displayErrorMessage(String error){
		JOptionPane.showMessageDialog(this, error);
	}
	
	public int getRank(){
		return this.rank;
	}
}
