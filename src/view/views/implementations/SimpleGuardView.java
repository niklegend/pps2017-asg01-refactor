package view.views.implementations;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.ViewGuardController.BackListener;
import controller.Implementations.ViewGuardController.ViewGuardListener;
import view.components.PrisonManagerJFrame;
import view.components.PrisonManagerJPanel;
import view.utils.SpringUtilities;
import view.views.interfaces.GuardView;

public class SimpleGuardView extends PrisonManagerJFrame implements GuardView {

    private static final long serialVersionUID = 86423038519594644L;

	private static final int ROWS_NUMBER = 5;
	private static final int COLUMNS_NUMBER = 2;
	private static final int INITIAL_X = 6;
	private static final int INITIAL_Y = 6;
	private static final int X_PADDING = 6;
	private static final int Y_PADDING = 6;
    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 230;
    private static final String NAME_LABEL = "Nome:		";
    private static final String SURNAME_LABEL = "Cognome:	";
    private static final String BIRTH_DATE_LABEL = "Data di nascita :	";
    private static final String RANK_LABEL = "Grado:	";
    private static final String TELEPHONE_NUMBER_LABEL = "Numero di telefono:	";

    private PrisonManagerJPanel south;
	private final JButton view = new JButton("Vedi profilo");
	private final JButton back = new JButton("Indietro");
	private PrisonManagerJPanel north;
	private final JLabel guardID = new JLabel("ID Guardia");
	private final JTextField guardIDField = new JTextField(2);
	private JLabel nameLabel;
	private JLabel nameInserted;
	private JLabel surnameLabel;
	private JLabel surnameInserted;
	private JLabel birthDateLabel;
	private JLabel birthDateInserted;
	private JLabel rankLabel;
	private JLabel rankInserted;
	private JLabel telephoneLabel;
	private JLabel telephoneInserted;
	private PrisonManagerJPanel center;
	private final int rank;
	
	/**
	 * costruttore
	 * @param rank il rank della guardia che sta visualizzando il programma
	 */
	public SimpleGuardView(int rank){
		this.rank=rank;
		this.getContentPane().setLayout(new BorderLayout());
		buildCenterPanel();
        buildNorthPanel();
        buildSouthPanel();
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.setVisible(true);
	}

    private void buildSouthPanel() {
        south = new PrisonManagerJPanel(new FlowLayout());
        south.add(view);
        south.add(back);
        this.getContentPane().add(BorderLayout.SOUTH,south);
    }

    private void buildNorthPanel() {
        north = new PrisonManagerJPanel(new FlowLayout());
        north.add(guardID);
        north.add(guardIDField);
        this.getContentPane().add(BorderLayout.NORTH,north);
    }

    private void buildCenterPanel() {
		center = new PrisonManagerJPanel(new SpringLayout());
		insertNameField();
		insertSurnameField();
		insertBirhdateField();
		insertRankField();
		insertTelephoneField();
		SpringUtilities.makeCompactGrid(center,
				ROWS_NUMBER, COLUMNS_NUMBER, //rows, cols
				INITIAL_X, INITIAL_Y,        //initX, initY
				X_PADDING, Y_PADDING);       //xPad, yPad
		this.getContentPane().add(BorderLayout.CENTER,center);
	}

	private void insertTelephoneField() {
		telephoneLabel = new JLabel(TELEPHONE_NUMBER_LABEL);
		telephoneInserted = new JLabel();
		center.add(telephoneLabel);
		center.add(telephoneInserted);
	}

	private void insertRankField() {
		rankLabel = new JLabel(RANK_LABEL);
		rankInserted = new JLabel();
		center.add(rankLabel);
		center.add(rankInserted);
	}

	private void insertBirhdateField() {
		birthDateLabel = new JLabel(BIRTH_DATE_LABEL);
		birthDateInserted = new JLabel();
		center.add(birthDateLabel);
		center.add(birthDateInserted);
	}

	private void insertSurnameField() {
		surnameLabel = new JLabel(SURNAME_LABEL);
		surnameInserted = new JLabel();
		center.add(surnameLabel);
		center.add(surnameInserted);
	}

	private void insertNameField() {
		nameLabel = new JLabel(NAME_LABEL);
		nameInserted = new JLabel();
		center.add(nameLabel);
		center.add(nameInserted);
	}

	public int getRank(){
		return this.rank;
	}
	
	public void addViewListener(ViewGuardListener viewListener){
		view.addActionListener(viewListener);
	}

	public void addBackListener(BackListener backListener){
		back.addActionListener(backListener);
	}
	public void displayErrorMessage(String errorMessage){
		JOptionPane.showMessageDialog(this, errorMessage);
	}
	
	public int getID(){
		if(guardIDField.getText().equals(""))
			return -1;
		return Integer.valueOf(guardIDField.getText());
	}
	
	public void setName(String name){
		this.nameInserted.setText(name);
	}
	
	public void setSurnameLabel(String surnameLabel){
		this.surnameInserted.setText(surnameLabel);
	}
	public void setBirth(String birth){
		this.birthDateInserted.setText(birth);
	}
	public void setRank(String rank){
		this.rankInserted.setText(rank);
	}
	public void setTelephoneLabel(String telephoneLabel){
		this.telephoneInserted.setText(telephoneLabel);
	}

}
