package view.views.implementations;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.ViewPrisonerController.BackListener;
import controller.Implementations.ViewPrisonerController.ViewProfileListener;
import view.components.PrisonManagerJFrame;
import view.components.PrisonManagerJPanel;
import view.utils.SpringUtilities;
import view.views.interfaces.PrisonerView;

public class SimplePrisonerView extends PrisonManagerJFrame implements PrisonerView {

    private static final String IMPRISONMENT_END_DATE_LABEL = "Fine reclusione:	";
    private static final long serialVersionUID = 7065438206105719545L;
    
	private static final int FRAME_WIDTH = 550;
	private static final int FRAME_HEIGHT = 350;
    private static final int ROWS_NUMBER = 5;
	private static final int COLUMNS_NUMBER = 2;
    private static final int INITIAL_X = 6;
    private static final int INITIAL_Y = 6;
    private static final int X_PADDING = 6;
    private static final int Y_PADDING = 6;
    private static final String NAME_LABEL = "Nome:		";
    private static final String SURNAME_LABEL = "Cognome:	";
    private static final String BIRTH_DATE_LABEL = "Data di nascita:	";
    private static final String IMPRISONMENT_START_DATE_LABEL = "Inizio reclusione:	";

	private PrisonManagerJPanel south;
	private final JButton viewProfileButton = new JButton("Vedi profilo");
	private final JButton backButton = new JButton("Indietro");
	private PrisonManagerJPanel northPanel;
	private final JLabel prisonerID = new JLabel("ID Prigioniero");
	private final JTextField prisonerIDField = new JTextField(2);
	private JLabel nameLabel;
	private JLabel name;
	private JLabel surnameLabel;
	private JLabel surname;
	private JLabel birthDateLabel;
	private JLabel birthDate;
	private JLabel startDateLabel;
	private JLabel startDate;
	private JLabel endDateLabel;
	private JLabel endDate;
	private PrisonManagerJPanel center;
	private PrisonManagerJPanel east;
	private JTextArea crimesTextArea;

	private final int rank;
	
	/**
	 * costruttore
	 * @param rank il rank della guardia che sta visualizzando il programma
	 */
	public SimplePrisonerView(int rank){
		this.rank=rank;
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		buildCenterPanel();
		buildNorthPanel();
		buildEastPanel();
		buildSouthPanel();
		this.setVisible(true);
	}

	private void buildSouthPanel() {
		south = new PrisonManagerJPanel(new FlowLayout());
		south.add(viewProfileButton);
		south.add(backButton);
		this.getContentPane().add(BorderLayout.SOUTH,south);
	}

	private void buildEastPanel() {
		east = new PrisonManagerJPanel(new FlowLayout());
		crimesTextArea = new JTextArea();
		crimesTextArea.setEditable(false);
		east.add(crimesTextArea);
		this.getContentPane().add(BorderLayout.EAST, east);
	}

	private void buildNorthPanel() {
		northPanel = new PrisonManagerJPanel(new FlowLayout());
		northPanel.add(prisonerID);
		northPanel.add(prisonerIDField);
		this.getContentPane().add(BorderLayout.NORTH, northPanel);
	}

	private void buildCenterPanel() {
		center = new PrisonManagerJPanel(new SpringLayout());
		insertNameLabel();
		insertSurnameLabel();
		insertBirthDateLabel();
		insertStartDateLabel();
		insertEndDateLabel();
		SpringUtilities.makeCompactGrid(center,
                ROWS_NUMBER, COLUMNS_NUMBER, //rows, cols
                INITIAL_X, INITIAL_Y,        //initX, initY
                X_PADDING, Y_PADDING);       //xPad, yPad
		this.getContentPane().add(BorderLayout.CENTER, center);
	}

	private void insertEndDateLabel() {
		endDateLabel = new JLabel(IMPRISONMENT_END_DATE_LABEL);
		endDate = new JLabel();
		center.add(endDateLabel);
		center.add(endDate);
	}

	private void insertStartDateLabel() {
		startDateLabel = new JLabel(IMPRISONMENT_START_DATE_LABEL);
		startDate = new JLabel();
		center.add(startDateLabel);
		center.add(startDate);
	}

	private void insertBirthDateLabel() {
		birthDateLabel = new JLabel(BIRTH_DATE_LABEL);
		birthDate = new JLabel();
		center.add(birthDateLabel);
		center.add(birthDate);
	}

	private void insertSurnameLabel() {
		surnameLabel = new JLabel(SURNAME_LABEL);
		surname = new JLabel();
		center.add(surnameLabel);
		center.add(surname);
	}

	private void insertNameLabel() {
		nameLabel = new JLabel(NAME_LABEL);
		name = new JLabel();
		center.add(nameLabel);
		center.add(name);
	}

	public void addViewListener(ViewProfileListener viewListener){
		viewProfileButton.addActionListener(viewListener);
	}

	public void addBackListener(BackListener backListener){
		backButton.addActionListener(backListener);
	}
	
	public int getID(){
		if(prisonerIDField.getText().equals(""))
			return -1;
		return Integer.valueOf(prisonerIDField.getText());
	}
	
	public void setProfile(String name, String surname, String birthDate, String start, String end){
		this.name.setText(name);
		this.surname.setText(surname);
		this.birthDate.setText(birthDate);
		this.startDate.setText(start);
		this.endDate.setText(end);
	}
	
	public void displayErrorMessage(String errorMessage){
		JOptionPane.showMessageDialog(this, errorMessage);
	}
	
	public int getRank(){
		return this.rank;
	}

	public void setCrimesTextArea(List<String> crimes){
		crimesTextArea.setText("");
		for(String crime : crimes){
			crimesTextArea.append(crime + "\n");
		}
	}
}
