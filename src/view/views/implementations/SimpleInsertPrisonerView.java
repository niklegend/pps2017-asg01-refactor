package view.views.implementations;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.InsertPrisonerController.AddCrimeListener;
import controller.Implementations.InsertPrisonerController.BackListener;
import controller.Implementations.InsertPrisonerController.InsertPrisonerListener;
import view.components.PrisonManagerJFrame;
import view.components.PrisonManagerJPanel;
import view.utils.SpringUtilities;

public class SimpleInsertPrisonerView extends PrisonManagerJFrame implements view.views.interfaces.InsertPrisonerView {

    private static final int START_DATE_FIELD_COLUMNS = 8;
    private static final long serialVersionUID = -3914632428464622887L;

	private static final int FRAME_WIDTH = 750;
	private static final int FRAME_HEIGHT = 400;
	private static final int EAST_PANEL_ROWS_NUMBER = 3;
	private static final int EAST_PANEL_COLUMNS_NUMBER = 1;
	private static final int EAST_PANEL_INITIAL_X = 6;
	private static final int EAST_PANEL_INITIAL_Y = 6;
	private static final int EAST_PANEL_X_PADDING = 6;
	private static final int EAST_PANEL_Y_PADDING = 6;
	private static final int CENTER_PANEL_ROWS_NUMBER = 7;
	private static final int CENTER_PANEL_COLUMNS_NUMBER = 2;
	private static final int CENTER_PANEL_INITIAL_X = 6;
	private static final int CENTER_PANEL_INITIAL_Y = 6;
	private static final int CENTER_PANEL_X_PADDING = 6;
	private static final int CENTER_PANEL_Y_PADDING = 6;
	private static final String DEFAULT_PRISONER_ID = "0";
    private static final String DEFAULT_BIRTH_DATE = "01/01/1980";
	private static final String DEFAULT_IMPRISONMENT_START_DATE = "01/01/2018";
	private static final String DEFAULT_IMPRISONMENT_END_DATE = "01/01/2018";
	private static final String DEFAULT_CELL_ID = "0";
	private static final int PRISONER_ID_FIELD_COLUMNS = 6;
	private static final int PRISONER_NAME_FIELD_COLUMNS = 6;
	private static final int PRISONER_SURNAME_FIELD_COLUMNS = 6;
	private static final int PRISONER_BIRTH_DATE_FIELD_COLUMNS = 6;

	private PrisonManagerJPanel south;
	private PrisonManagerJPanel east;
	private PrisonManagerJPanel center;
	private PrisonManagerJPanel north;

	private final JButton insert = new JButton("Inserisci");
	private final JLabel prisonerIDLabel = new JLabel("ID Prigioniero");
	private final JTextField prisonerIDField = new JTextField(PRISONER_ID_FIELD_COLUMNS);
	private final JLabel nameLabel = new JLabel("Nome");
	private final JTextField nameField = new JTextField(PRISONER_NAME_FIELD_COLUMNS);
	private final JLabel surname = new JLabel("Cognome");
	private final JTextField surnameField = new JTextField(PRISONER_SURNAME_FIELD_COLUMNS);
	private final JLabel birthDate = new JLabel("Data di nascita (mm/gg/aaaa)");
	private final JTextField birthDateField = new JTextField(PRISONER_BIRTH_DATE_FIELD_COLUMNS);
	private final JLabel startDateLabel = new JLabel("Inizio incarcerazione (mm/gg/aaaa)");
	private final JTextField startDateField = new JTextField(START_DATE_FIELD_COLUMNS);
	private final JLabel endDateLabel = new JLabel("Fine incarcerazione (mm/gg/aaaa)");
	private final JTextField endDateField = new JTextField(8);
	private final JLabel cellIDLabel = new JLabel("ID Cella");
	private final JTextField cellIDField = new JTextField(8);

	private final JButton back = new JButton("Indietro");
	private final JLabel title = new JLabel("Inserisci un prigioniero");
    private JComboBox<?> type;
    private JTextArea textArea;
    private final JButton add=new JButton("Aggiungi un crimine");
	private final String[] crimes = {"Reati contro gli animali","Reati associativi","Blasfemia e sacrilegio","Reati economici e finanziari","Falsa testimonianza","Reati militari","Reati contro il patrimonio","Reati contro la persona","Reati nell' ordinamento italiano","Reati tributari","Traffico di droga","Casi di truffe"};

	private final String datePattern = "MM/dd/yyyy";
    private final SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
    private Date date;

    private final int rank;
	
    /**
     * costruttore
     * @param rank il rank della guardia che sta visualizzando il programma
     */
	public SimpleInsertPrisonerView(int rank){
		this.rank=rank;
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
        buildNorthPanel();
        buildEastPanel();
        buildCenterPanel();
        buildSouthPanel();
		this.setVisible(true);
	}

    private void buildSouthPanel() {
        south = new PrisonManagerJPanel(new FlowLayout());
        south.add(insert);
        south.add(back);
        this.getContentPane().add(BorderLayout.SOUTH,south);
    }

    private void buildCenterPanel() {
        center = new PrisonManagerJPanel(new SpringLayout());
		insertIDField();
		insertNameField();
		insertSurnameField();
		insertBirthDateField();
		insertStartDateField();
		insertEndDateField();
		insertCellIDField();
        SpringUtilities.makeCompactGrid(center,
                CENTER_PANEL_ROWS_NUMBER, CENTER_PANEL_COLUMNS_NUMBER, //rows, cols
                CENTER_PANEL_INITIAL_X, CENTER_PANEL_INITIAL_Y,        //initX, initY
                CENTER_PANEL_X_PADDING, CENTER_PANEL_Y_PADDING);       //xPad, yPad
        this.getContentPane().add(BorderLayout.CENTER,center);
    }

	private void insertCellIDField() {
		center.add(cellIDLabel);
		center.add(cellIDField);
		cellIDField.setText(DEFAULT_CELL_ID);
	}

	private void insertEndDateField() {
		center.add(endDateLabel);
		center.add(endDateField);
		endDateField.setText(DEFAULT_IMPRISONMENT_END_DATE);
	}

	private void insertStartDateField() {
		center.add(startDateLabel);
		center.add(startDateField);
		startDateField.setText(DEFAULT_IMPRISONMENT_START_DATE);
	}

	private void insertBirthDateField() {
		center.add(birthDate);
		center.add(birthDateField);
		birthDateField.setText(DEFAULT_BIRTH_DATE);
	}

	private void insertSurnameField() {
		center.add(surname);
		center.add(surnameField);
	}

	private void insertNameField() {
		center.add(nameLabel);
		center.add(nameField);
	}

	private void insertIDField() {
		center.add(prisonerIDLabel);
		center.add(prisonerIDField);
		prisonerIDField.setText(DEFAULT_PRISONER_ID);
	}

	private void buildEastPanel() {
        type = new JComboBox<>(crimes);
        type.setSelectedItem(0);
        textArea = new JTextArea();
        textArea.setEditable(false);
        east = new PrisonManagerJPanel(new SpringLayout());
        east.add(type);
        east.add(textArea);
        JScrollPane logScrollPane = new JScrollPane(add);
        east.add(logScrollPane);
        SpringUtilities.makeCompactGrid(east,
                EAST_PANEL_ROWS_NUMBER, EAST_PANEL_COLUMNS_NUMBER, //rows, cols
                EAST_PANEL_INITIAL_X, EAST_PANEL_INITIAL_Y,        //initX, initY
                EAST_PANEL_X_PADDING, EAST_PANEL_Y_PADDING);       //xPad, yPad
        this.getContentPane().add(BorderLayout.EAST,east);
    }

    private void buildNorthPanel() {
        north = new PrisonManagerJPanel(new FlowLayout());
        north.add(title);
        this.getContentPane().add(BorderLayout.NORTH,north);
    }

    public void addInsertPrisonerListener(InsertPrisonerListener addPrisonerListener){
		insert.addActionListener(addPrisonerListener);
	}
	
	public void addBackListener(BackListener backListener){
		back.addActionListener(backListener);
	}

	public int getPrisonerIDLabel() {
		return Integer.valueOf(prisonerIDField.getText());
	}

	public String getName() {
		return nameField.getText();
	}

	public String getSurname() {
		return surnameField.getText();
	}

	public Date getImprisonmentStartDate() {
		try {
			date = dateFormat.parse(startDateField.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public Date getImprisonmentEndDate() {
		try {
			date = dateFormat.parse(endDateField.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	public Date getBirthDate() {
		try {
			date = dateFormat.parse(birthDateField.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	public int getCellID() {
		return Integer.valueOf(this.cellIDField.getText());
	}
	
	public void displayMessage(String error){
		JOptionPane.showMessageDialog(this, error);
	}
	
	public int getRank(){
		return this.rank;
	}
	
	public void setList(List<String>list){
		textArea.setText("");
		for(String s : list){
			textArea.append(s+"\n");
		}
	}
	
	public List<String> getList(){
		String s[] = textArea.getText().split("\\r?\\n");
		return new ArrayList<>(Arrays.asList(s));
	}
	
	public String getCombo(){
		return Objects.requireNonNull(type.getSelectedItem()).toString();
	}
	
	public void addAddCrimeListener(AddCrimeListener addCrimeListener){
		add.addActionListener(addCrimeListener);
	}
	
}
