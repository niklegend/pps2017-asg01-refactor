package view.views.implementations;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.AddMovementController.BackListener;
import controller.Implementations.AddMovementController.InsertListener;
import view.components.PrisonManagerJFrame;
import view.components.PrisonManagerJPanel;
import view.utils.SpringUtilities;

/**
 * classe view dove si aggiungo movimenti di bilancio
 */
public class SimpleAddMovementView extends PrisonManagerJFrame implements view.views.interfaces.AddMovementView {

    private static final long serialVersionUID = -3724774108126619974L;

    private static final int ROWS_NUMBER = 3;
    private static final int COLUMNS_NUMBER = 2;
    private static final int INITIAL_X = 6;
    private static final int INITIAL_Y = 6;
    private static final int X_PADDING = 6;
    private static final int Y_PADDING = 6;
    private static final int FRAME_WIDTH = 250;
    private static final int FRAME_HEIGHT = 300;

    private PrisonManagerJPanel center;
    private PrisonManagerJPanel south;
    private PrisonManagerJPanel north;

    private final JLabel title= new JLabel("Aggiungi un movimento");
    private final JLabel amount = new JLabel("Ammontare");
    private final JLabel symbol = new JLabel("+ : -");
    private final JLabel description = new JLabel("Descrizione");
    private final JTextField inputField = new JTextField(6);
    private final JTextField inputDescriptionField = new JTextField(6);
    private JComboBox<?> actionSignsBox;
    private final String[] actionSigns = {"+", "-"};
	private final JButton back = new JButton("Indietro");
	private final JButton insert = new JButton("Inserisci");

	private final int rank;
	
	/**
	 * costruttore
	 * @param rank il rank della guardia che sta visualizzando il programma
	 */
	public SimpleAddMovementView(int rank)
	{
		this.rank=rank;
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		buildNorthPanel();
        buildCenterPanel();
		buildSouthPanel();
		this.setVisible(true);
		 
	}

	private void buildNorthPanel(){
        north = new PrisonManagerJPanel(new FlowLayout());
        north.add(title);
        this.getContentPane().add(BorderLayout.NORTH,north);
    }

	private void buildCenterPanel(){
        center = new PrisonManagerJPanel(new SpringLayout());
        actionSignsBox = new JComboBox<>(actionSigns);
        actionSignsBox.setSelectedItem(1);
        center.add(symbol);
        center.add(actionSignsBox);
        center.add(amount);
        center.add(inputField);
        center.add(description);
        center.add(inputDescriptionField);
        SpringUtilities.makeCompactGrid(center,
                ROWS_NUMBER, COLUMNS_NUMBER, 	//rows, cols
                INITIAL_X, INITIAL_Y,           //initX, initY
                X_PADDING, Y_PADDING);          //xPad, yPad
        this.getContentPane().add(BorderLayout.CENTER,center);
    }

	private void buildSouthPanel(){
        south = new PrisonManagerJPanel(new FlowLayout());
        south.add(insert);
        south.add(back);
        this.getContentPane().add(BorderLayout.SOUTH,south);
    }

	public int getRank(){
		return this.rank;
	}
	
	public void addBackListener(BackListener backListener){
		back.addActionListener(backListener);
	}
	
	public void addInsertListener(InsertListener insertListener){
		insert.addActionListener(insertListener);
	}
	
	public String getDescription(){
		return inputDescriptionField.getText();
	}
	
	public double getActionSigns(){
			return Double.valueOf(inputField.getText());
	}
	
	public String getSymbol(){		
		return String.valueOf(actionSignsBox.getSelectedItem());
	}
	
	public void displayMessage(String error){
		JOptionPane.showMessageDialog(this, error);
	}

}
