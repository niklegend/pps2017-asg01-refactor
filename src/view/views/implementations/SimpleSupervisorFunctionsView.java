package view.views.implementations;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SpringLayout;

import controller.Implementations.SupervisorController.BackListener;
import controller.Implementations.SupervisorController.InsertGuardListener;
import controller.Implementations.SupervisorController.RemoveGuardListener;
import controller.Implementations.SupervisorController.ShowPrisonersListener;
import controller.Implementations.SupervisorController.ViewGuardListener;
import view.components.PrisonManagerJFrame;
import view.components.PrisonManagerJPanel;
import view.utils.SpringUtilities;

public class SimpleSupervisorFunctionsView extends PrisonManagerJFrame implements view.views.interfaces.SupervisorFunctionsView {

	private static final long serialVersionUID = 1240198114577795025L;

	private static final int FRAME_WIDTH = 580;
	private static final int FRAME_HEIGHT = 200;
	private static final int ROWS_NUMBER = 2;
	private static final int COLUMNS_NUMBER = 2;
	private static final int INITIAL_Y = 6;
	private static final int INITIAL_X = 6;
	private static final int X_PADDING = 6;
	private static final int Y_PADDING = 6;

	private PrisonManagerJPanel north;
    private PrisonManagerJPanel center;
    private PrisonManagerJPanel south;

	private final JLabel title = new JLabel("Funzioni riservate");
	private final JButton addGuard = new JButton("Aggiungi guardia");
	private final JButton removeGuard = new JButton("Rimuovi guardia");
	private final JButton viewGuard = new JButton("Vedi guardia");
	private final JButton viewPrisoners = new JButton("Vedi prigionieri in una frazione di tempo");
	private final JButton back = new JButton("Indietro");

	private final int rank;
	
	/**
	 * costruttore
	 * @param rank il rank della guardia che sta visualizzando il programma
	 */
	public SimpleSupervisorFunctionsView(int rank){
		this.rank=rank;
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		buildNorthPanel();
		buildCenterPanel();
		buildSouthPanel();
		this.setVisible(true);
	}

	private void buildSouthPanel() {
		south = new PrisonManagerJPanel(new FlowLayout());
		south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH,south);
	}

	private void buildCenterPanel() {
		center = new PrisonManagerJPanel(new SpringLayout());
		center.add(addGuard);
		center.add(removeGuard);
		center.add(viewGuard);
		center.add(viewPrisoners);
		SpringUtilities.makeCompactGrid(center,
				ROWS_NUMBER, COLUMNS_NUMBER, //rows, cols
				INITIAL_X, INITIAL_Y,        //initX, initY
				X_PADDING, Y_PADDING);       //xPad, yPad
		this.getContentPane().add(BorderLayout.CENTER,center);
	}

	private void buildNorthPanel() {
		north = new PrisonManagerJPanel(new FlowLayout());
		north.add(title);
		this.getContentPane().add(BorderLayout.NORTH,north);
	}

	public int getRank() {
		return rank;
	}
	
	public void addBackListener(BackListener backListener){
		back.addActionListener(backListener);
	}

	public void addShowPrisonersListener(ShowPrisonersListener showPrisonersListeners){
		viewPrisoners.addActionListener(showPrisonersListeners);
	}
	
	public void addInsertGuardListener(InsertGuardListener insertGuardListener){
		addGuard.addActionListener(insertGuardListener);
	}
	
	public void addRemoveGuardListener(RemoveGuardListener removeListener){
		removeGuard.addActionListener(removeListener);
	}
	
	public void addViewGuardListener(ViewGuardListener viewListener){
		viewGuard.addActionListener(viewListener);
	}
}
