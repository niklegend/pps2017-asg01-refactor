package view.views.implementations;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.AddVisitorsController.InsertListener;
import controller.Implementations.AddVisitorsController.BackListener;
import model.Implementations.SimpleVisitor;
import model.builders.SimpleVisitorBuilder;
import view.components.PrisonManagerJFrame;
import view.components.PrisonManagerJPanel;
import view.utils.SpringUtilities;

/**
 * view in cui si aggiungono visitatori
 */
public class SimpleAddVisitorsView extends PrisonManagerJFrame implements view.views.interfaces.AddVisitorsView {

	private static final long serialVersionUID = -8964073612262207713L;

	private static final String DEFAULT_BIRTH_DATE = "01/01/2017";
	private static final String DEFAULT_PRISONER_ID = "0";

	private final int rank;
	private final String pattern = "MM/dd/yyyy";

	private static final int FRAME_WIDTH = 450;
    private static final int FRAME_HEIGHT = 400;
    private static final int ROWS_NUMBER = 4;
    private static final int COLUMNS_NUMBER = 2;
    private static final int INITIAL_X = 6;
    private static final int INITIAL_Y = 6;
    private static final int X_PADDING = 6;
    private static final int Y_PADDING = 6;


    private final SimpleDateFormat format = new SimpleDateFormat(pattern);
	private PrisonManagerJPanel south;
	private final JButton insert = new JButton("Inserisci");
	private PrisonManagerJPanel north;
	private final JLabel nameLabel = new JLabel("Nome : ");
	private final JTextField nameField = new JTextField(6);
	private final JLabel surnameLabel = new JLabel("Cognome :");
	private final JTextField surnameField = new JTextField(6);
	private final JLabel birthDateLabel = new JLabel("Data di nascita (mm/gg/aaaa) : ");
	private final JTextField birthDateField = new JTextField(6);
	private PrisonManagerJPanel center;
	private final JButton back = new JButton("Indietro");
	private final JLabel title = new JLabel("Inserisci visitatore");
	private final JLabel prisonerIDLabel = new JLabel("Id prigioniero incontrato  : ");
	private final JTextField prisonerIDField = new JTextField(6);
	
	/**
	 * costruttore
	 * @param rank il rank della guardia che sta visualizzando il programma
	 */
	public SimpleAddVisitorsView(int rank)
	{	
		this.rank=rank;
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		buildNorthPanel();
		buildCenterPanel();
        buildSouthPanel();
		this.setVisible(true);
	}

    private void buildSouthPanel() {
        south = new PrisonManagerJPanel(new FlowLayout());
        south.add(insert);
        south.add(back);
        this.getContentPane().add(BorderLayout.SOUTH,south);
	}

    private void buildNorthPanel(){
        north = new PrisonManagerJPanel(new FlowLayout());
        north.add(title);
        this.getContentPane().add(BorderLayout.NORTH,north);
    }
    
    private void buildCenterPanel(){
        center = new PrisonManagerJPanel(new SpringLayout());
        center.add(nameLabel);
        center.add(nameField);
        center.add(surnameLabel);
        center.add(surnameField);
        center.add(birthDateLabel);
        center.add(birthDateField);
        birthDateField.setText(DEFAULT_BIRTH_DATE);
        center.add(prisonerIDLabel);
        center.add(prisonerIDField);
        prisonerIDField.setText(DEFAULT_PRISONER_ID);
        SpringUtilities.makeCompactGrid(center,
                ROWS_NUMBER, COLUMNS_NUMBER, //rows, cols
                INITIAL_X, INITIAL_Y,        //initX, initY
                X_PADDING, Y_PADDING);       //xPad, yPad
        this.getContentPane().add(BorderLayout.CENTER, center); 
    }
	
	public SimpleVisitor getVisitor(){
		Date date=null;
		try {
			date = format.parse(birthDateField.getText());
		} catch (ParseException e) {
			displayMessage("Data non correttamente formattata!");
		}
        return new SimpleVisitorBuilder().setName(nameField.getText()).setSurname(surnameField.getText()).setBirthDate(date).setPrisonerID(Integer.valueOf(prisonerIDField.getText())).createSimpleVisitor();
	}

	public int getRank() {
		return this.rank;
	}

	public void displayMessage(String errorMessage){
		JOptionPane.showMessageDialog(this, errorMessage);
	}
	
	public void addBackListener(BackListener backListener){
		back.addActionListener(backListener);
	}
	
	public void addInsertVisitorListener(InsertListener insertListener){
		insert.addActionListener(insertListener);
	}
	 
	
	

}
