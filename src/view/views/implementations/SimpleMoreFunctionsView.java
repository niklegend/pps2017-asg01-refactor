package view.views.implementations;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SpringLayout;

import controller.Implementations.MoreFunctionsController.AddMovementListener;
import controller.Implementations.MoreFunctionsController.AddVisitorsListener;
import controller.Implementations.MoreFunctionsController.BackListener;
import controller.Implementations.MoreFunctionsController.BalanceListener;
import controller.Implementations.MoreFunctionsController.Chart1Listener;
import controller.Implementations.MoreFunctionsController.Chart2Listener;
import controller.Implementations.MoreFunctionsController.ViewCellsListener;
import controller.Implementations.MoreFunctionsController.ViewVisitorsListener;
import view.components.PrisonManagerJFrame;
import view.components.PrisonManagerJPanel;
import view.utils.SpringUtilities;

public class SimpleMoreFunctionsView extends PrisonManagerJFrame implements view.views.interfaces.MoreFunctionsView {

	private static final long serialVersionUID = -960406183837784254L;

	private static final int FRAME_WIDTH = 500;
	private static final int FRAME_HEIGHT = 250;
	private static final int ROWS_NUMBER = 4;
	private static final int COLUMNS_NUMBER = 2;
	private static final int INITIAL_X = 6;
	private static final int INITIAL_Y = 6;
	private static final int X_PADDING = 6;
	private static final int Y_PADDING = 6;

	private PrisonManagerJPanel northPanel;
	private final JLabel title = new JLabel("Altre funzioni");
	private PrisonManagerJPanel centerPanel;
	private final JButton addMovementButton = new JButton("Aggiungi un movimento");
	private final JButton viewBalance=new JButton("Guarda bilancio");
	private final JButton viewFirstChart=new JButton("Grafico prigionieri per anno");
	private final JButton viewSecondChart=new JButton("Grafico percentuale crimini");
	private final JButton addVisitors = new JButton("Aggiungi un visitatore");
	private final JButton viewVisitors = new JButton("Controlla visitatori");
	private final JButton viewCells = new JButton("Guarda celle");
	private PrisonManagerJPanel southPanel;
	private final JButton backButton = new JButton("Indietro");
	private final int rank;
	
	/**
	 * costruttore
	 * @param rank il rank della guardia che sta visualizzando il programma
	 */
	public SimpleMoreFunctionsView(int rank){
		this.rank=rank;
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		buildNorthPanel();
		buildCenterPanel();
		buildSouthPanel();
		this.setVisible(true);
	}

	private void buildSouthPanel() {
		southPanel = new PrisonManagerJPanel(new FlowLayout());
		southPanel.add(backButton);
		this.getContentPane().add(BorderLayout.SOUTH, southPanel);
	}

	private void buildCenterPanel() {
		centerPanel = new PrisonManagerJPanel(new SpringLayout());
		centerPanel.add(addMovementButton);
		centerPanel.add(viewBalance);
		centerPanel.add(viewFirstChart);
		centerPanel.add(viewSecondChart);
		centerPanel.add(addVisitors);
		centerPanel.add(viewVisitors);
		centerPanel.add(viewCells);
		centerPanel.add(backButton);
		SpringUtilities.makeCompactGrid(centerPanel,
				ROWS_NUMBER, COLUMNS_NUMBER, //rows, cols
				INITIAL_X, INITIAL_Y,        //initX, initY
				X_PADDING, Y_PADDING);       //xPad, yPad
		this.getContentPane().add(BorderLayout.CENTER, centerPanel);
	}

	private void buildNorthPanel() {
		northPanel = new PrisonManagerJPanel(new FlowLayout());
		northPanel.add(title);
		this.getContentPane().add(BorderLayout.NORTH, northPanel);
	}

	public int getRank(){
		return this.rank;
	}
	
	public void addBackListener(BackListener backListener){
		backButton.addActionListener(backListener);
	}
	
	public void addAddMovementListener(AddMovementListener addMovementListener){
		addMovementButton.addActionListener(addMovementListener);
	}
	
	public void addBalanceListener(BalanceListener balanceListener){
		viewBalance.addActionListener(balanceListener);
	}
	
	public void addChart1Listener(Chart1Listener chart1Listener){
		viewFirstChart.addActionListener(chart1Listener);
	}

	public void addChart2Listener(Chart2Listener chart2Listener){
		viewSecondChart.addActionListener(chart2Listener);
	}

	public void addAddVisitorsListener(AddVisitorsListener addVisitorsListener){
		addVisitors.addActionListener(addVisitorsListener);
	}
	
	public void addViewVisitorsListener(ViewVisitorsListener viewVisitorsListener){
		viewVisitors.addActionListener(viewVisitorsListener);
	}
	
	public void addViewCellsListener(ViewCellsListener viewCellsListener){
		viewCells.addActionListener(viewCellsListener);
	}
}
