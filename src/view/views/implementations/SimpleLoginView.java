package view.views.implementations;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import controller.Implementations.LoginController.LoginListener;
import view.components.PrisonManagerJFrame;
import view.components.PrisonManagerJPanel;

public class SimpleLoginView extends PrisonManagerJFrame implements view.views.interfaces.LoginView {

	private static final long serialVersionUID = -9055948983228935131L;

	private static final int FRAME_WIDTH = 400;
	private static final int FRAME_HEIGHT = 130;

	private PrisonManagerJPanel south;
	private final JButton login = new JButton("Login");
	private PrisonManagerJPanel center;
	private final JLabel usernameLabel = new JLabel("Username");
	private final JTextField usernameField = new JTextField(8);
	private final JLabel passwordLabel = new JLabel("Password");
	private final JPasswordField passwordField = new JPasswordField(8);
	private PrisonManagerJPanel north;
	private final JLabel title = new JLabel("Prison Manager");

	public SimpleLoginView(){
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		buildSouthPanel();
		buildCenterPanel();
		buildNorthPanel();
		this.setVisible(true);
	}

	private void buildNorthPanel() {
		north = new PrisonManagerJPanel(new FlowLayout());
		north.add(title);
		this.getContentPane().add(BorderLayout.NORTH,north);
	}

	private void buildCenterPanel() {
		center = new PrisonManagerJPanel(new FlowLayout());
		center.add(usernameLabel);
		center.add(usernameField);
		center.add(passwordLabel);
		center.add(passwordField);
		this.getContentPane().add(BorderLayout.CENTER,center);
	}

	private void buildSouthPanel() {
		south = new PrisonManagerJPanel(new FlowLayout());
		south.add(login);
		this.getContentPane().add(BorderLayout.SOUTH,south);
	}

	public String getUsernameLabel(){
		return usernameField.getText();
	}
	
	public String getPasswordLabel(){
		return new String(passwordField.getPassword());
	}
	
	public void displayErrorMessage(String error){
		JOptionPane.showMessageDialog(this, error);
	}
	
	public void addLoginListener(LoginListener loginListener){
		login.addActionListener(loginListener);
	}
}
