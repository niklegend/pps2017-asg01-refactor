package view.views.interfaces;

import controller.Implementations.ViewVisitorsController;

import javax.swing.*;

public interface VisitorsView {

    void addBackListener(ViewVisitorsController.BackListener backListener);

    void createTable(JTable table);

    void dispose();

    int getRank();

}
