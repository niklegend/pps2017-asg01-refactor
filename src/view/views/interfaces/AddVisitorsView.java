package view.views.interfaces;

import controller.Implementations.AddVisitorsController;
import model.Interfaces.Visitor;

public interface AddVisitorsView {

    void addBackListener(AddVisitorsController.BackListener backListener);

    void addInsertVisitorListener(AddVisitorsController.InsertListener insertListener);

    void dispose();

    int getRank();

    Visitor getVisitor();

    void displayMessage(String s);
}
