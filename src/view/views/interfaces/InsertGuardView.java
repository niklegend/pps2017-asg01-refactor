package view.views.interfaces;

import controller.Implementations.InsertGuardController;
import model.Interfaces.Guard;

public interface InsertGuardView {

    void addBackListener(InsertGuardController.BackListener backListener);

    void addInsertListener(InsertGuardController.InsertListener insertListener);

    void dispose();

    int getRank();

    Guard getGuard();

    void displayErrorMessage(String errorMessage);
}
