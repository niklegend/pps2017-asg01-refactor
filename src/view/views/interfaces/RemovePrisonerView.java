package view.views.interfaces;


import controller.Implementations.RemovePrisonerController;

public interface RemovePrisonerView {

    void addRemoveListener(RemovePrisonerController.RemoveListener removeListener);

    void addBackListener(RemovePrisonerController.BackListener backListener);

    int getID();

    void displayErrorMessage(String errorMessage);

    void dispose();

    int getRank();
}
