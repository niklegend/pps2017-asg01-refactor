package view.views.interfaces;

import controller.Implementations.SupervisorController;

public interface SupervisorFunctionsView {

    void addBackListener(SupervisorController.BackListener backListener);

    void addShowPrisonersListener(SupervisorController.ShowPrisonersListener showPrisonersListener);

    void addInsertGuardListener(SupervisorController.InsertGuardListener insertGuardListener);

    void addRemoveGuardListener(SupervisorController.RemoveGuardListener removeGuardListener);

    void addViewGuardListener(SupervisorController.ViewGuardListener viewGuardListener);

    void dispose();

    int getRank();
}
