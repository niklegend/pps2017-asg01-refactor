package view.views.interfaces;

import controller.Implementations.InsertPrisonerController;

import java.util.Date;
import java.util.List;

public interface InsertPrisonerView {

    void addInsertPrisonerListener(InsertPrisonerController.InsertPrisonerListener insertPrisonerListener);

    void addBackListener(InsertPrisonerController.BackListener backListener);

    void addAddCrimeListener(InsertPrisonerController.AddCrimeListener addCrimeListener);

    String getName();

    String getSurname();

    Date getBirthDate();

    int getPrisonerIDLabel();

    void dispose();

    int getRank();

    void setList(List<String> list);

    void displayMessage(String insertedPrisoner);

    List<String> getList();

    String getCombo();

    Date getImprisonmentStartDate();

    Date getImprisonmentEndDate();

    int getCellID();
}
