package view.views.interfaces;

import controller.Implementations.ShowPrisonersController;

import javax.swing.*;
import java.util.Date;

public interface ShowPrisonersView {

    void addBackListener(ShowPrisonersController.BackListener backListener);

    void addComputeListener(ShowPrisonersController.ComputeListener computeListener);

    void dispose();

    int getRank();

    Date getFrom();

    Date getTo();

    void createTable(JTable table);
}
