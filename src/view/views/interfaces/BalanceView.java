package view.views.interfaces;

import controller.Implementations.BalanceController;

import javax.swing.*;

public interface BalanceView {

    void dispose();

    int getRank();

    void setLabel(String s);

    void createTable(JTable table);

    void addBackListener(BalanceController.BackListener backListener);
}
