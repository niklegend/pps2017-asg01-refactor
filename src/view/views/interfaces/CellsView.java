package view.views.interfaces;

import controller.Implementations.ViewCellsController;

import javax.swing.*;

public interface CellsView {

    void addBackListener(ViewCellsController.BackListener backListener);

    void createTable(JTable table);

    void dispose();

    int getRank();
}
