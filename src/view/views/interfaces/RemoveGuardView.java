package view.views.interfaces;

import controller.Implementations.RemoveGuardController;

public interface RemoveGuardView {

    void addBackListener(RemoveGuardController.BackListener backListener);

    void addRemoveGuardListener(RemoveGuardController.RemoveGuardListener removeGuardListener);

    int getID();

    void displayErrorMessage(String errorMessage);

    void dispose();

    int getRank();
}
