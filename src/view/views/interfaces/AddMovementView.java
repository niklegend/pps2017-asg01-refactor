package view.views.interfaces;

import controller.Implementations.AddMovementController;

public interface AddMovementView {

    void addBackListener(AddMovementController.BackListener backListener);

    void addInsertListener(AddMovementController.InsertListener insertListener);

    void dispose();

    int getRank();

    String getDescription();

    double getActionSigns();

    String getSymbol();

    void displayMessage(String invalidInput);
}
