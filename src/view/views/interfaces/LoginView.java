package view.views.interfaces;

import controller.Implementations.LoginController;

public interface LoginView {

    void addLoginListener(LoginController.LoginListener loginListener);

    void displayErrorMessage(String errorMessage);

    String getUsernameLabel();

    String getPasswordLabel();

    void dispose();
}
