package view.views.interfaces;

import controller.Implementations.MoreFunctionsController;

public interface MoreFunctionsView {

    void addBackListener(MoreFunctionsController.BackListener backListener);

    void addAddMovementListener(MoreFunctionsController.AddMovementListener addMovementListener);

    void addBalanceListener(MoreFunctionsController.BalanceListener balanceListener);

    void addChart1Listener(MoreFunctionsController.Chart1Listener chart1Listener);

    void addChart2Listener(MoreFunctionsController.Chart2Listener chart2Listener);

    void addAddVisitorsListener(MoreFunctionsController.AddVisitorsListener addVisitorsListener);

    void addViewVisitorsListener(MoreFunctionsController.ViewVisitorsListener viewVisitorsListener);

    void addViewCellsListener(MoreFunctionsController.ViewCellsListener viewCellsListener);

    void dispose();

    int getRank();
}
