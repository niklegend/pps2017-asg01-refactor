package view.views.interfaces;

import controller.Implementations.MainController;

public interface MainView {

    void addLogoutListener(MainController.LogoutListener logoutListener);

    void addInsertPrisonerListener(MainController.InsertPrisonerListener insertPrisonerListener);

    void addRemovePrisonerListener(MainController.RemovePrisonerListener removePrisonerListener);

    void addViewPrisonerListener(MainController.ViewPrisonerListener viewPrisonerListener);

    void addMoreFunctionsListener(MainController.MoreFunctionsListener moreFunctionsListener);

    void addSupervisorListener(MainController.SupervisorListener supervisorListener);

    void dispose();

    int getRank();
}
