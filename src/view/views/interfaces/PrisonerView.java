package view.views.interfaces;

import controller.Implementations.ViewPrisonerController;

import java.util.List;

public interface PrisonerView {


    void addViewListener(ViewPrisonerController.ViewProfileListener viewProfileListener);

    void addBackListener(ViewPrisonerController.BackListener backListener);

    int getID();

    void displayErrorMessage(String errorMessage);

    void setProfile(String name, String surname, String birthDate,
                    String imprisonmentStartDate, String imprisonmentEndDate);

    void setCrimesTextArea(List<String> crimes);

    void dispose();

    int getRank();
}
