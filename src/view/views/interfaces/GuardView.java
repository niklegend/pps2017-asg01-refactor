package view.views.interfaces;

import controller.Implementations.ViewGuardController;

public interface GuardView {

    int getID();

    void setName(String name);

    void setSurnameLabel(String surnameLabel);

    void setBirth(String s);

    void setRank(String s);

    void setTelephoneLabel(String telephoneNumber);

    void displayErrorMessage(String guardia_non_trovata);

    void dispose();

    int getRank();

    void addBackListener(ViewGuardController.BackListener backListener);

    void addViewListener(ViewGuardController.ViewGuardListener viewGuardListener);
}
