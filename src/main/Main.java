package main;

import controller.Implementations.LoginController;
import view.views.implementations.SimpleLoginView;

/**
 * The main of the application.
 */
final class Main {
	
	/**
     * Program main, this is the "root" of the application.
     * @param args
     * unused,ignore
     */
	 public static void main(final String... args) {
		 new LoginController(new SimpleLoginView()).initializeResources();
	 }

}
