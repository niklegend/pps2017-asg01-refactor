package model.Interfaces;

public interface Movement {

    char getSign();

    double getAmount();

    String getDescription();

    String getDate();
}
