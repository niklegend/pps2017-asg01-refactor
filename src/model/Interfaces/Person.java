package model.Interfaces;

import java.util.Date;
/**
 * questa interfaccia rappresenta una persona
 */
public interface Person{

	/**
	 * Metodo che ritorna il nome della persona.
	 * 
	 * @return the name
	 */
    String getName();

	/**
	 * Metodo che ritorna il cognome della persona.
	 * 
	 * @return the surname
	 */

    String getSurname();

	/**
	 * Metodo che ritorna la data di nascita dellla persona.
	 * 
	 * @return the birth date
	 */

    Date getBirthDate();

}
