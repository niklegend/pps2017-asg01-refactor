package model.Interfaces;

/**
 * questa interfaccia rappresenta un visitatore
 */
public interface Visitor extends Person{

	/**
	 * metodo che ritorna l'id del prigioniero che il visitatore è andato a trovare
	 * @return id del prigioniero visitato
	 */
    int getPrisonerID();

}
