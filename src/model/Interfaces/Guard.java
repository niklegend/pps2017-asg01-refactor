package model.Interfaces;

/**
 * interfaccia di una guardia
 */
public interface Guard extends Person{

    /**
	 * Metodo che ritorna la password
	 * @return the password
	*/
	String getPassword();
	
	/**
	 * Metodo che ritorna l username
	 * @return the username
	*/
	int getUsername();
	
	/**
	 *metodo che ritorna il numero di telefono
	 *@return the telephone number 
	 */
	String getTelephoneNumber();

    /**
	 *metodo che ritorna il grado della guardia
	 *@return the rank
	 */
	int getRank();

    /**
	 * metodo che restituisce l'id della guardia
	 * @return id della guardia
	 */
	int getID();
	
}
