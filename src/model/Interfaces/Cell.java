package model.Interfaces;

/**
 * interfaccia di una cella
 * @author Utente
 *
 */
public interface Cell {

    int getId();

    int getCurrentPrisoners();

    void setCurrentPrisoners(int currentPrisoners);

    int getCapacity();

    String getPosition();
}
