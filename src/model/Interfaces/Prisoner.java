package model.Interfaces;

import java.util.Date;
import java.util.List;

/**
 * interfaccia che rappresenta un prigioniero
 */
public interface Prisoner extends Person{

    /**
	 * Metodo che ritrna la lista dei crimini commessi da un criminale
	 * @return la lista dei crimini
	*/
    List<String> getCrimes();
	
	/**
	 * Metodo che ritorna l'id del prigioniero
	 * @return id del prigioniero
	*/
    int getPrisonerID();

    /**
	 * Metodo che ritorna la data di inizio della reclusione
	 * @return data di inizio della reclusione
	*/
    Date getImprisonmentStartDate();

    /**
	 * Metodo che ritorna la data di fine della reclusione
	 * @return data di fine della reclusione
	*/
    Date getImprisonmentEndDate();

    /**
	 * Metodo che ritorna l'id della cella 
	 * @return id della cella
	 */
    int getCellID();

}