package model.Implementations;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import model.Interfaces.Prisoner;
import model.templates.BasePerson;

/**
 * implementazione di un prigioniero
 */
public class SimplePrisoner extends BasePerson implements Serializable, Prisoner{

	private static final long serialVersionUID = -3204660779285410481L;
	
	private final int prisonerID;
	
	private final Date imprisonmentStartDate;
	
	private final Date imprisonmentEndDate;
	
	private final List<String> crimes;
	
	private final int cellID;

	/**
	 * Instantiates a new prisoner.
	 *
	 * @param name the name
	 * @param surname the surname
	 * @param birthDate the birth date
	 * @param prisonerID the id
	 * @param imprisonmentStartDate inizio della reclusione
	 * @param imprisonmentEndDate imprisonmentEndDate della reclusione
	 */
	public SimplePrisoner(String name, String surname, Date birthDate, int prisonerID, Date imprisonmentStartDate, Date imprisonmentEndDate, List<String>list, int cellID) {
		super(name, surname, birthDate);
		this.prisonerID = prisonerID;
		this.imprisonmentStartDate=imprisonmentStartDate;
		this.imprisonmentEndDate = imprisonmentEndDate;
		this.crimes =list;
		this.cellID = cellID;
	}

    @Override
	public List<String> getCrimes(){
		return this.crimes;
	}

	public int getPrisonerID() {
		return this.prisonerID;
	}

    public Date getImprisonmentStartDate() {
		return this.imprisonmentStartDate;
	}

    public Date getImprisonmentEndDate() {
		return imprisonmentEndDate;
	}

    public int getCellID() {
		return cellID;
	}

    @Override
	public String toString() {
		return "SimplePrisoner [prisonerID=" + prisonerID + ", inizio=" + imprisonmentStartDate + ", imprisonmentEndDate=" + imprisonmentEndDate + ", crimes="
				+ crimes + "]";
	}

}
