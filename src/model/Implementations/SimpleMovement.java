package model.Implementations;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import model.Interfaces.Movement;

/**
 * implementazione di un movimento
 */
public class SimpleMovement implements Serializable, Movement {

	private static final long serialVersionUID = -8676396152502823263L;
	
	private final String description;
	private final double amount;
	private final String date;
	private final char sign;
	
	/**
	 * costruttore di movimento
	 * @param description descrizione
	 * @param amount ammontare
	 * @param sign segno
	 */
	public SimpleMovement(String description, double amount, char sign){
		this.amount=amount;
		this.description=description;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		date = dateFormat.format(c.getTime());
		this.sign = sign;
	}
	public String getDescription() {
		return this.description;
	}

	public double getAmount() {
		return this.amount;
	}

	public char getSign() {
		return this.sign;
	}
	
	public String getDate(){
		return this.date;
	}
	
	public String toString() {
		return "Movement [description=" + description + "sign"+ sign + ", amount=" + amount + ", date=" + date + "]";
	}
}
