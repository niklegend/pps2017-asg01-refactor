package model.Implementations;

import java.io.Serializable;

import model.Interfaces.Cell;

/**
 * implementazione di una cella
 */
public class SimpleCell implements Cell, Serializable{

	private static final long serialVersionUID = 9167940013424894676L;

	private final int id;
	private final String position;
	private final int capacity;
	private int currentPrisoners;
	
	/**
	 * costruttore cella
	 * @param id id cella
	 * @param position posizione
	 * @param capacity capacità
	 */
	public SimpleCell(int id, String position, int capacity){
		this.id=id;
		this.position=position;
		this.capacity=capacity;
		this.currentPrisoners=0;
	}

	public int getId() {
		return id;
	}

    public String getPosition() {
		return position;
	}

    public int getCapacity() {
		return capacity;
	}

    public int getCurrentPrisoners() {
		return currentPrisoners;
	}

	public void setCurrentPrisoners(int currentPrisoners) {
		this.currentPrisoners = currentPrisoners;
	}

	@Override
	public String toString() {
		return "SimpleCell [id=" + id + ", position=" + position + ", capacity=" + capacity + ", CurrentPrisoners="
				+ currentPrisoners + "]";
	}
	
	
}
