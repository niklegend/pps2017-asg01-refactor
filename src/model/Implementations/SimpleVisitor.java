package model.Implementations;

import java.io.Serializable;
import java.util.Date;

import model.Interfaces.Visitor;
import model.templates.BasePerson;

/**
 * Implementazione di un visitatore
 */
public class SimpleVisitor extends BasePerson implements Serializable, Visitor{

	private static final long serialVersionUID = 5306827736761721189L;

	/**
	 * id del prigioniero
	 */
	private final int prisonerID;
	
	/**
	 * 
	 * @param name nome
	 * @param surname cognome
	 * @param birthDate data di nascita
	 * @param prisonerID id del prigioniero andato a trovare
	 */
	public SimpleVisitor(String name, String surname, Date birthDate, int prisonerID) {
		super(name, surname, birthDate);
		this.prisonerID=prisonerID;
	}

	@Override
	public int getPrisonerID() {
		return this.prisonerID;
	}

    @Override
	public String toString() {
		return "SimpleVisitor getName()=" + getName() + ", getSurname()="+ getSurname() + ", getBirthDate()=" + getBirthDate() + "[getIdPrisoner()=" + getPrisonerID() + "]";
	}


	

}
