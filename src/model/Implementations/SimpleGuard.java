package model.Implementations;

import java.io.Serializable;
import java.util.Date;

import model.Interfaces.Guard;
import model.templates.BasePerson;

/**
 * implementazione di una guardia
 */
public class SimpleGuard extends BasePerson implements Serializable, Guard{
	
	private static final long serialVersionUID = 3975704240795357459L;

	private int rank;
	private String telephoneNumber;
	private final int guardID;
	private final String password;
	
	/**
	 * costruttore di una guardia
	 * @param name nome
	 * @param surname cognome
	 * @param birthDate data di nascita
	 * @param rank grado
	 * @param telephoneNumber numero di telefono
	 * @param guardID id guardia
	 * @param password password
	 */
	public SimpleGuard(String name, String surname, Date birthDate, int rank, String telephoneNumber, int guardID,
					   String password) {
		super(name, surname, birthDate);
		this.setRank(rank);
		this.setTelephoneNumber(telephoneNumber);
		this.guardID = guardID;
		this.password = password;
	}

    @Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public int getUsername() {
		return this.guardID;
	}

	@Override
	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	private void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	@Override
	public int getRank() {
		return rank;
	}
	
	private void setRank(int rank) {
		this.rank = rank;
	}
	
	@Override
	public int getID() {
		return guardID;
	}


    @Override
	public String toString() {
		return "SimpleGuard [rank=" + rank + ", telephoneNumber=" + telephoneNumber + ", guardID=" + guardID
				+ "]";
	}

}
