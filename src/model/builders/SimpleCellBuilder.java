package model.builders;

import model.Implementations.SimpleCell;

public class SimpleCellBuilder {
    private int id;
    private String position;
    private int capacity;

    public SimpleCellBuilder setId(int id) {
        this.id = id;
        return this;
    }

    public SimpleCellBuilder setPosition(String position) {
        this.position = position;
        return this;
    }

    public SimpleCellBuilder setCapacity(int capacity) {
        this.capacity = capacity;
        return this;
    }

    public SimpleCell createSimpleCell() {
        return new SimpleCell(id, position, capacity);
    }
}