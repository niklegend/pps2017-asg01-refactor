package model.builders;

import model.Implementations.SimpleMovement;

public class SimpleMovementBuilder {
    private String description;
    private double amount;
    private char sign;

    public SimpleMovementBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public SimpleMovementBuilder setAmount(double amount) {
        this.amount = amount;
        return this;
    }

    public SimpleMovementBuilder setSign(char sign) {
        this.sign = sign;
        return this;
    }

    public SimpleMovement createSimpleMovement() {
        return new SimpleMovement(description, amount, sign);
    }
}