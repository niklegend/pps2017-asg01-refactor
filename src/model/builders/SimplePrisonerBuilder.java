package model.builders;

import model.Implementations.SimplePrisoner;

import java.util.Date;
import java.util.List;

public class SimplePrisonerBuilder {
    private String name;
    private String surname;
    private Date birthDate;
    private int prisonerID;
    private Date imprisonmentStartDate;
    private Date imprisonmentEndDate;
    private List<String> list;
    private int cellID;

    public SimplePrisonerBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public SimplePrisonerBuilder setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public SimplePrisonerBuilder setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public SimplePrisonerBuilder setPrisonerID(int prisonerID) {
        this.prisonerID = prisonerID;
        return this;
    }

    public SimplePrisonerBuilder setInizio(Date imprisonmentStartDate) {
        this.imprisonmentStartDate = imprisonmentStartDate;
        return this;
    }

    public SimplePrisonerBuilder setImprisonmentEndDate(Date imprisonmentEndDate) {
        this.imprisonmentEndDate = imprisonmentEndDate;
        return this;
    }

    public SimplePrisonerBuilder setList(List<String> list) {
        this.list = list;
        return this;
    }

    public SimplePrisonerBuilder setCellID(int cellID) {
        this.cellID = cellID;
        return this;
    }

    public SimplePrisoner createSimplePrisoner() {
        return new SimplePrisoner(name, surname, birthDate, prisonerID, imprisonmentStartDate, imprisonmentEndDate, list, cellID);
    }
}