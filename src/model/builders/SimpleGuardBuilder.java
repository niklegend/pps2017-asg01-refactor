package model.builders;

import model.Implementations.SimpleGuard;

import java.util.Date;

public class SimpleGuardBuilder {
    private String name;
    private String surname;
    private Date birthDate;
    private int rank;
    private String telephoneNumber;
    private int guardID;
    private String password;

    public SimpleGuardBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public SimpleGuardBuilder setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public SimpleGuardBuilder setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public SimpleGuardBuilder setRank(int rank) {
        this.rank = rank;
        return this;
    }

    public SimpleGuardBuilder setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
        return this;
    }

    public SimpleGuardBuilder setGuardID(int guardID) {
        this.guardID = guardID;
        return this;
    }

    public SimpleGuardBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public SimpleGuard createSimpleGuard() {
        return new SimpleGuard(name, surname, birthDate, rank, telephoneNumber, guardID, password);
    }
}