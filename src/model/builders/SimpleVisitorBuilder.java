package model.builders;

import model.Implementations.SimpleVisitor;

import java.util.Date;

public class SimpleVisitorBuilder {
    private String name;
    private String surname;
    private Date birthDate;
    private int prisonerID;

    public SimpleVisitorBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public SimpleVisitorBuilder setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public SimpleVisitorBuilder setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public SimpleVisitorBuilder setPrisonerID(int prisonerID) {
        this.prisonerID = prisonerID;
        return this;
    }

    public SimpleVisitor createSimpleVisitor() {
        return new SimpleVisitor(name, surname, birthDate, prisonerID);
    }
}