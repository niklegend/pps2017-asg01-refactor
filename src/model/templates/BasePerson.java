package model.templates;

import java.io.Serializable;
import java.util.Date;

import model.Interfaces.Person;

/**
 * implementazione di una persona
 */
public abstract class BasePerson implements Person, Serializable {
	
	private final String name;
	private final String surname;
	private final Date birthDate;
	
	/**
	 * Instantiates a new person.
	 *
	 * @param name the name
	 * @param surname the surname
	 * @param birthDate the birth date
	 */
    protected BasePerson(String name, String surname, Date birthDate) {
		super();
		this.name = name;
		this.surname = surname;
		this.birthDate = birthDate;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	@Override
	public String toString() {
		return "BasePerson [name=" + this.getName() + ", surname=" + this.getSurname() + ", birthDate=" + this.getBirthDate() + "]";
	}
	
	
}
