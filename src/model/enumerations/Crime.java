package model.enumerations;

public enum Crime {

    AGAINST_ANIMALS("Reati contro gli animali"),
    ASSOCIATIVE_OFFENSES("Reati associativi"),
    BLASPHEMY_AND_SACRILEGE("Blasfemia e sacrilegio"),
    ECONOMIC_AND_FINANCIAL("Reati economici e finanziari"),
    FALSE_TESTIMONY("Falsa testimonianza"),
    MILITARY("Reati militari"),
    AGAINST_PROPERTY("Reati contro il patrimonio"),
    AGAINST_THE_PERSON("Reati contro la persona"),
    AGAINST_THE_ITALIAN_LEGAL_SYSTEM("Reati nell' ordinamento italiano"),
    TAX_OFFENSES("Reati tributari"),
    DRUG_TRAFFICKING("Traffico di droga"),
    CASES_OF_SCAMS("Casi di truffe");


    private final String crimeName;

    Crime(String crimeName){
        this.crimeName = crimeName;
    }

    public String getName(){
        return crimeName;
    }
}
