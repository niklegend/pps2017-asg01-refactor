package controller.templates;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseController<T>{

    private static final String RESOURCE_FOLDER_PATH = "res/";
    static final String CONFIG_FILE_PATH = "config.json";

    public static String getResourcesFolderPath(){
        return RESOURCE_FOLDER_PATH;
    }

    protected List<T> readObjectsFromFile(File file) {
        //se il file è vuoto ritorno una lista vuota
        List<T> objects = new ArrayList<>();
        try {
            FileInputStream fi = new FileInputStream(file);
            ObjectInputStream oi = new ObjectInputStream(fi);
            //salvo il contenuto del file in una lista
            while(oi.available() >= 0){
                @SuppressWarnings("unchecked")
                T s = (T) oi.readObject();
                objects.add(s);
            }
            fi.close();
            oi.close();
        } catch (final IOException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return objects;
    }


    void writeObjectsToFile(List<T> objects, File file){
        try {
            FileOutputStream fo = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fo);
            //ciclo tutte le guardie nella lista e li scrivo su file
            writeObjects(objects, fo, os);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    protected void saveObjectsFromScratch(List<T> objects, File file) throws IOException{
        FileOutputStream fo = new FileOutputStream(file);
        ObjectOutputStream os = new ObjectOutputStream(fo);
        //elimino il vecchio contenuto
        os.flush();
        fo.flush();
        //salvo nel file la lista aggiornata
        writeObjects(objects, fo, os);
    }

    private void writeObjects(List<T> objects, FileOutputStream fo, ObjectOutputStream os) throws IOException {
        for (T s : objects) {
            os.writeObject(s);
        }
        os.close();
        fo.close();
    }

    protected static void createResourcesFolder() throws IOException{
        //creo cartella in cui mettere i dati da salvare
        String resourcesFolderPath = getResourcesFolderPath();
        if (Files.notExists(Paths.get(resourcesFolderPath)) &&
                !new File(resourcesFolderPath).mkdir()){
            throw new IOException("Unable to create resources folder");
        }
    }

    protected static void createConfigFile() throws IOException{
        if (Files.notExists(Paths.get(CONFIG_FILE_PATH))){
            throw new IOException("Missing config file");
        }
    }

}

