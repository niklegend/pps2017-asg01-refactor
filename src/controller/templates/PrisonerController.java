package controller.templates;

import model.Interfaces.Cell;
import model.Interfaces.Prisoner;

import java.io.*;
import java.util.List;

import static controller.templates.CellsController.getCellsFilePath;

public abstract class PrisonerController extends BaseController {

    private static final String PRISONERS_FILE_PATH = getResourcesFolderPath() + "Prisoners.txt";
    private static final String CURRENT_PRISONERS_FILE_PATH = getResourcesFolderPath() + "CurrentPrisoners.txt";

    @SuppressWarnings("unchecked")
    protected List<Prisoner> getPrisoners(){
        return readObjectsFromFile(new File(PRISONERS_FILE_PATH));
    }

    /**
     * salva le liste dei prigionieri
     * @param prisoners prigionieri di sempre
     * @param currentPrisoners prigionieri correnti
     */
    @SuppressWarnings("unchecked")
    protected void setPrisoners(List<Prisoner> prisoners, List<Prisoner> currentPrisoners){
        writeObjectsToFile(prisoners, new File(PRISONERS_FILE_PATH));
        writeObjectsToFile(currentPrisoners, new File(CURRENT_PRISONERS_FILE_PATH));
    }

    @SuppressWarnings("unchecked")
    protected void setCurrentPrisoners(List<Prisoner> currentPrisoners){
        writeObjectsToFile(currentPrisoners, new File(CURRENT_PRISONERS_FILE_PATH));
    }

    @SuppressWarnings("unchecked")
    protected List<Prisoner> getCurrentPrisoners(){
        return readObjectsFromFile(new File(CURRENT_PRISONERS_FILE_PATH));
    }

    public static String getPrisonerFilePath() {
        return PRISONERS_FILE_PATH;
    }

    /**
     * ritorna la situazione delle celle in una lista
     * @return lista con le celle
     */
    @SuppressWarnings("unchecked")
    protected List<Cell> getCells(){
        return readObjectsFromFile(new File(getCellsFilePath()));
    }

    /**
     * salva la lista di celle aggiornata
     * @param list lista di celle
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    protected void setCells(List<Cell> list) throws IOException{
        saveObjectsFromScratch(list, new File(getCellsFilePath()));
    }

}
