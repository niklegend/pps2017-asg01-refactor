package controller.templates;

import model.builders.SimpleGuardBuilder;
import model.Interfaces.Guard;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class GuardsController extends BaseController<Guard> {

    private static final String GUARDS_FILE_PATH = getResourcesFolderPath() + "GuardieUserPass.txt";
    private static final String DATE_FORMAT_PATTERN = "MM/dd/yyyy";

    /**
     * metodo che restutuisce la lista di guardie salvata su file
     * @return lista di guardie
     */
    protected List<Guard> getGuards(){
        return readObjectsFromFile(new File(GUARDS_FILE_PATH));
    }

    /**
     * salva le guardie
     * @param guards lista di guardie
     */
    protected void setGuards(List<Guard> guards){
        writeObjectsToFile(guards, new File(GUARDS_FILE_PATH));
    }

    @SuppressWarnings("SameReturnValue")
    private static String getGuardsFilePath(){
        return GUARDS_FILE_PATH;
    }

    protected void createGuardsFile()throws IOException{
        //creo file per le guardie
        File guardsFile = new File(getGuardsFilePath());
        //se il file non è stato inizializzato lo faccio ora
        if(guardsFile.length()==0){
            initializeGuards(guardsFile);
        }
    }

    /**
     * metodo che inizializza le guardie
     * @param file file in cui creare le guardie
     * @throws IOException
     */
    private void initializeGuards(File file) throws IOException{
        Date date = getDate();
        List<Guard> guardList = new ArrayList<>();
        guardList.add(createFirstGuard(date));
        guardList.add(createSecondGuard(date));
        guardList.add(createThirdGuard(date));
        saveObjectsFromScratch(guardList, file);
    }

    private static Date getDate() {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_PATTERN);
        Date date = null;
        try {
            date = format.parse("01/01/1980");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private static Guard createThirdGuard(Date date) {
        return new SimpleGuardBuilder()
                    .setName("Gennaro")
                    .setSurname("Alfieri")
                    .setBirthDate(date).setRank(3)
                    .setTelephoneNumber("0764568")
                    .setGuardID(3)
                    .setPassword("qwerty")
                    .createSimpleGuard();
    }

    private static Guard createSecondGuard(Date date) {
        return new SimpleGuardBuilder()
                    .setName("Emile")
                    .setSurname("Heskey")
                    .setBirthDate(date)
                    .setRank(2)
                    .setTelephoneNumber("456789")
                    .setGuardID(2)
                    .setPassword("asdasd")
                    .createSimpleGuard();
    }

    private static Guard createFirstGuard(Date date) {
        return new SimpleGuardBuilder()
                    .setName("Oronzo")
                    .setSurname("Cantani")
                    .setBirthDate(date)
                    .setRank(1)
                    .setTelephoneNumber("0764568")
                    .setGuardID(1)
                    .setPassword("ciao01")
                    .createSimpleGuard();
    }

    protected void saveGuardList(List<Guard> guards) throws IOException {
        saveObjectsFromScratch(guards, new File(GUARDS_FILE_PATH));
    }
}
