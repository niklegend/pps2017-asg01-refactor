package controller.templates;

import model.Interfaces.Prisoner;
import model.Interfaces.Visitor;
import view.views.interfaces.AddVisitorsView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.Objects;

import static controller.templates.PrisonerController.*;

public abstract class VisitorsController extends BaseController {

    private final static String VISITORS_FILE_PATH = getResourcesFolderPath() + "Visitors.txt";


    /**
     * ritorna la lista dei visitatori
     * @return lista dei visitatori
     */
    @SuppressWarnings("unchecked")
    protected List<Visitor> getVisitors(){
        return readObjectsFromFile(new File(VISITORS_FILE_PATH));
    }

    @SuppressWarnings("unchecked")
    protected List<Prisoner> getPrisoners(){
        return readObjectsFromFile(new File(getPrisonerFilePath()));
    }

    /**
     * salva la lista dei visitatori aggiornata
     * @param visitors lista dei visitatori
     */
    protected void setVisitors(List<Visitor> visitors, AddVisitorsView visitorsView){
        File f = new File(VISITORS_FILE_PATH);
        FileOutputStream fo = null;
        ObjectOutputStream os = null;
        try {
            fo = new FileOutputStream(f);
            os = new ObjectOutputStream(fo);
        } catch (IOException e) {
            visitorsView.displayMessage(e.getMessage());
        }
        //salvo su file la lista aggiornata di visitatori
        for(Visitor g1 : visitors){
            try {
                Objects.requireNonNull(os).writeObject(g1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            Objects.requireNonNull(os).close();
            Objects.requireNonNull(fo).close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
