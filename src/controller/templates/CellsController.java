package controller.templates;

import model.builders.SimpleCellBuilder;
import model.Interfaces.Cell;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public abstract class CellsController extends BaseController<Cell> {

    private static final String CELLS_FILE_PATH = getResourcesFolderPath() + "Celle.txt";
    private static final String CELL_NUMBER_LABEL = "cell_number";
    private static final String FIRST_FLOOR_LABEL = "Primo piano";
    private static final String SECOND_FLOOR_LABEL = "Secondo piano";
    private static final String THIRD_FLOOR_LABEL = "Terzo piano";
    private static final int FIRST_FLOOR_CAPACITY = 4;
    private static final int SECOND_FLOOR_CAPACITY = 3;
    private static final int THIRD_FLOOR_CAPACITY = 4;
    private static final String UNDERGROUND_FLOOR_LABEL = "Piano sotterraneo, celle di isolamento";
    private static final int UNDERGROUND_FLOOR_CAPACITY = 1;
    private static final int FIRST_FLOOR_CELLS_NUMBER = 20;
    private static final int SECOND_FLOOR_CELLS_NUMBER = 40;
    private static final int THIRD_FLOOR_CELLS_NUMBER = 45;


    protected List<Cell> getCells(){
        return readObjectsFromFile(new File(CELLS_FILE_PATH));
    }

    public static String getCellsFilePath(){
        return CELLS_FILE_PATH;
    }

    public static int getCellsNumber() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        JSONObject configurations = (JSONObject) parser.parse(new FileReader(CONFIG_FILE_PATH));
        return (int) configurations.get(CELL_NUMBER_LABEL);
    }

    public static void createCellsFile() throws IOException, ParseException {
        //leggo il file contenente le celle
        File f = new File(getCellsFilePath());
        //se il file non è ancora stato inizializzato lo faccio ora
        if(f.length()==0){
            initializeCells(f);
        }
    }

    /**
     * metodo che inizializza le celle
     * @param file file in cui creare le celle
     * @throws IOException
     */
    private static void initializeCells(File file) throws IOException, ParseException {
        List<Cell> cellList = buildCellsStructure();
        FileOutputStream fo = new FileOutputStream(file);
        ObjectOutputStream os = new ObjectOutputStream(fo);
        os.flush();
        fo.flush();
        for(Cell cell : cellList){
            os.writeObject(cell);
        }
        os.close();
    }

    private static List<Cell> buildCellsStructure() throws IOException, ParseException {
        List<Cell> cellList = new ArrayList<>();
        Cell c;
        for(int i = 0; i < getCellsNumber(); i++){
            if(i < FIRST_FLOOR_CELLS_NUMBER){
                c = createFirstFloorCell(i);
            }
            else if(i < SECOND_FLOOR_CELLS_NUMBER){
                c = createSecondFloorCell(i);
            }
            else if(i < THIRD_FLOOR_CELLS_NUMBER){
                c = createThirdFloorCell(i);
            }
            else{
                c = createUndergroundFloorCell(i);
            }
            cellList.add(c);
        }
        return cellList;
    }

    private static Cell createUndergroundFloorCell(int i) {
        Cell c;
        c = new SimpleCellBuilder()
                .setId(i)
                .setPosition(UNDERGROUND_FLOOR_LABEL)
                .setCapacity(UNDERGROUND_FLOOR_CAPACITY)
                .createSimpleCell();
        return c;
    }

    private static Cell createThirdFloorCell(int i) {
        Cell c;
        c = new SimpleCellBuilder()
                .setId(i)
                .setPosition(THIRD_FLOOR_LABEL)
                .setCapacity(THIRD_FLOOR_CAPACITY)
                .createSimpleCell();
        return c;
    }

    private static Cell createSecondFloorCell(int i) {
        return new SimpleCellBuilder()
                .setId(i)
                .setPosition(SECOND_FLOOR_LABEL)
                .setCapacity(SECOND_FLOOR_CAPACITY)
                .createSimpleCell();
    }

    private static Cell createFirstFloorCell(int i) {
        return new SimpleCellBuilder()
                .setId(i)
                .setPosition(FIRST_FLOOR_LABEL)
                .setCapacity(FIRST_FLOOR_CAPACITY)
                .createSimpleCell();
    }
}
