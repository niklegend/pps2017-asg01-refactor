package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.JFrame;

import controller.templates.BaseController;
import model.enumerations.Crime;
import model.Interfaces.Prisoner;
import view.components.AWTBarChart;
import view.components.AWTPieChart;
import view.views.implementations.SimpleAddMovementView;
import view.views.implementations.SimpleAddVisitorsView;
import view.views.implementations.SimpleBalanceView;
import view.views.implementations.SimpleMainView;
import view.views.implementations.SimpleCellsView;
import view.views.implementations.SimpleVisitorsView;
import view.views.interfaces.MoreFunctionsView;

/**
 * controller della more functions view
 */
public class MoreFunctionsController extends BaseController<Prisoner> {

    private static final String CURRENT_PRISONERS_FILE_PATH = getResourcesFolderPath() + "CurrentPrisoners.txt";
    private static final String PRISONERS_FILE_PATH = getResourcesFolderPath() + "Prisoners.txt";
    private static final int PRISON_OPENING_YEAR = 2017;

    private final MoreFunctionsView moreFunctionsView;


    /**
	  * costruttore
	  * @param moreFunctionsView la view
	  */
	public MoreFunctionsController(MoreFunctionsView moreFunctionsView){
		this.moreFunctionsView=moreFunctionsView;
		moreFunctionsView.addBackListener(new BackListener());
		moreFunctionsView.addAddMovementListener(new AddMovementListener());
		moreFunctionsView.addBalanceListener(new BalanceListener());
		moreFunctionsView.addChart1Listener(new Chart1Listener());
		moreFunctionsView.addChart2Listener(new Chart2Listener());
		moreFunctionsView.addAddVisitorsListener(new AddVisitorsListener());
		moreFunctionsView.addViewVisitorsListener(new ViewVisitorsListener());
		moreFunctionsView.addViewCellsListener(new ViewCellsListener());
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 * @author Utente
	 *
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			moreFunctionsView.dispose();
			new MainController(new SimpleMainView(moreFunctionsView.getRank()));
		}
		
	}
	
	/**
	 * listener che apre la add movement view
	 */
	public class AddMovementListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			moreFunctionsView.dispose();
			new AddMovementController(new SimpleAddMovementView(moreFunctionsView.getRank()));
		}
		
	}
	
	/**
	 * listener che apre la balance view
	 */
	public class BalanceListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			moreFunctionsView.dispose();
			new BalanceController(new SimpleBalanceView(moreFunctionsView.getRank()));
		}
		
	}
	
	private void createChart1(){
		//creo una mappa contenente gli anni e il numero dei prigioneri in quell anno
		Map<Integer,Integer> chartData = new TreeMap<>();
        //salvo la lista di prigionieri
		List<Prisoner> list = readObjectsFromFile(new File(PRISONERS_FILE_PATH));
		//recupero l'anno massimo in cui un prigioniero è imprigionato
		int max = getMax(Objects.requireNonNull(list));
        getChart1Data(chartData, list, max);
        //creo il grafico
		AWTBarChart chart = new AWTBarChart(chartData,"Numero prigionieri per anno",
				"Numero prigionieri per anno");
		chart.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

    private void getChart1Data(Map<Integer, Integer> chartData, List<Prisoner> list, int max) {
        //ciclo tutti gli anni e modifico il numero di prigionieri
        for(int i = PRISON_OPENING_YEAR; i <= max; i++){
            int num = 0;
            for(Prisoner p:list){
                Calendar calendar = Calendar.getInstance();
                Calendar calendar2 = Calendar.getInstance();
                calendar.setTime(p.getImprisonmentStartDate());
                calendar2.setTime(p.getImprisonmentEndDate());
                if(calendar.get(Calendar.YEAR)<=i&&calendar2.get(Calendar.YEAR)>=i){
                    num++;
                }
            }
            chartData.put(i, num);
        }
    }

    private int getMax(List<Prisoner> list){
		int max=0;
		for(Prisoner p: list){
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(p.getImprisonmentEndDate());
			if(calendar.get(Calendar.YEAR)>max){
				max=calendar.get(Calendar.YEAR);
			}
		}
		return max;
	}
	
	/**
	 * listener che si occupa di aprire il primo grafico 
	 */
	public class Chart1Listener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			createChart1();
		}
	}
	
	private void createChart2(){
		//recupero i prigionieri correnti in una lista
        List<Prisoner> prisonerList = readObjectsFromFile(new File(CURRENT_PRISONERS_FILE_PATH));
		// creo una mappa in cui inserire come chiave il reato e
        // come valore il numero di prigionieri che l'anno commesso
        Map<String, Integer> chartData = getChart2Data(prisonerList);
		//creo il grafico
		AWTPieChart pie = new AWTPieChart("Percentuale crimini commessi dai reclusi attuali", chartData);
		pie.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

    private Map<String, Integer> getChart2Data(List<Prisoner> prisonerList) {
        Map<String,Integer> chartData = new HashMap<>();
        Stream.of(Crime.values()).forEach(crime -> chartData.put(crime.getName(), 0));
        for(Prisoner p: Objects.requireNonNull(prisonerList)){
            for(String s : p.getCrimes()){
                if(getAllCrimes().contains(s)){
                    chartData.put(s, chartData.get(s)+1);
                }
            }
        }
        return chartData;
    }

	private List<String> getAllCrimes() {
		return Arrays.stream(Crime.values())
				.map(Crime::getName)
				.collect(Collectors.toList());
	}

	/**
	 * listener che si occupa di lanciare il secondo grafico
	 */
	public class Chart2Listener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			createChart2();
		}
		
	}
	
	/**
	 * listener che apre l'add visitors view
	 */
	public class AddVisitorsListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			moreFunctionsView.dispose();
			new AddVisitorsController(new SimpleAddVisitorsView(moreFunctionsView.getRank()));
		}
		
	}
	
	/**
	 * listener che apre la view visitors view
	 */
	public class ViewVisitorsListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			moreFunctionsView.dispose();
			new ViewVisitorsController(new SimpleVisitorsView(moreFunctionsView.getRank()));
		}
		
	}
	
	/**
	 * listener che apre la view cells view
	 */
	public class ViewCellsListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			moreFunctionsView.dispose();
			new ViewCellsController(new SimpleCellsView(moreFunctionsView.getRank()));
		}
		
	}
}
