package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import controller.templates.PrisonerController;
import model.builders.SimplePrisonerBuilder;
import model.Interfaces.Cell;
import model.Interfaces.Prisoner;
import org.json.simple.parser.ParseException;
import view.views.implementations.SimpleMainView;
import view.views.interfaces.InsertPrisonerView;

import static controller.templates.CellsController.*;

/**
 * controller della view insert-prisoner
 */
public class InsertPrisonerController extends PrisonerController {

    private static InsertPrisonerView insertPrisonerView;

	/**
	 * costruttore
	 * @param insertPrisonerView la view
	 */
	public InsertPrisonerController(InsertPrisonerView insertPrisonerView) {
		InsertPrisonerController.insertPrisonerView=insertPrisonerView;
		insertPrisonerView.addInsertPrisonerListener(new InsertPrisonerListener());
		insertPrisonerView.addBackListener(new BackListener());
		insertPrisonerView.addAddCrimeListener(new AddCrimeListener());
		
	}
	
	private void insertPrisonerData(List<Prisoner> prisoners, List<Prisoner> currentPrisoners,
                                    Prisoner prisoner){
        //controllo che non ci siano errori
        if(isSomethingEmpty(prisoner)){
			insertPrisonerView.displayMessage("Completa tutti i campi");
		}
		else{
            Calendar today = getCalendar();
			if(isNotIDInserted(prisoners, prisoner)){
				if(areNotDatesCorrect(prisoner.getImprisonmentStartDate(),
                        prisoner.getImprisonmentEndDate(),
						prisoner.getBirthDate(), today)){
					insertPrisonerView.displayMessage("Correggi le date");
				}
				else savePrisoner(prisoners, currentPrisoners, prisoner);
			}
		}
	}

    private void savePrisoner(List<Prisoner> prisoners, List<Prisoner> currentPrisoners, Prisoner p) {
        //aggiungo nuovo prigioniero in entrambe le liste
        prisoners.add(p);
        currentPrisoners.add(p);
        //recupero le celle salvate
        List<Cell> cellList = getCells();
        if (setCurrentPrisoners(p, cellList)) return;
        //salvo la lista di celle aggiornata
		try {
			setCells(cellList);
		} catch (IOException exception) {
			insertPrisonerView.displayMessage(exception.getMessage());
		}
		//salvo le liste di prigionieri aggiornate
        setPrisoners(prisoners, currentPrisoners);
        insertPrisonerView.displayMessage("Prigioniero inserito");
        insertPrisonerView.setList(new ArrayList<>());
    }

    private Calendar getCalendar() {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        return today;
    }

    private boolean setCurrentPrisoners(Prisoner p, List<Cell> list) {
        for(Cell c : list){
            try {
                if((p.getCellID() == c.getId()) || (p.getCellID() < 0) ||
                        (p.getCellID() >= getCellsNumber())){
                    //controllo che la cella inserita non sia piena
                    if (isCellFull(c)) return true;
                        //aggiungo un membro alla cella
                        c.setCurrentPrisoners(c.getCurrentPrisoners() + 1);
                }
            } catch (IOException | ParseException e) {
                insertPrisonerView.displayMessage(e.getMessage());
            }
        }
        return false;
    }

    private boolean isCellFull(Cell c) {
        if(c.getCapacity() == c.getCurrentPrisoners()){
            insertPrisonerView.displayMessage("Prova con un'altra cella");
            return true;
        }
        return false;
    }

    private Prisoner createPrisoner() {
        return new SimplePrisonerBuilder()
                    .setName(insertPrisonerView.getName())
                    .setSurname(insertPrisonerView.getSurname())
                    .setBirthDate(insertPrisonerView.getBirthDate())
                    .setPrisonerID(insertPrisonerView.getPrisonerIDLabel())
                    .setInizio(insertPrisonerView.getImprisonmentStartDate())
                    .setImprisonmentEndDate(insertPrisonerView.getImprisonmentEndDate())
                    .setList(insertPrisonerView.getList())
                    .setCellID(insertPrisonerView.getCellID())
                    .createSimplePrisoner();
    }

    private boolean isNotIDInserted(List<Prisoner> prisoners, Prisoner p) {
	    boolean correct = true;
        for(Prisoner p1 : prisoners){
            if(p1.getPrisonerID()==p.getPrisonerID()){
                insertPrisonerView.displayMessage("ID già usato");
                correct=false;
            }
        }
        return correct;
    }

    /**
	 * listener che si occupa di inserire prigionieri
	 */
	public class InsertPrisonerListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertPrisonerData(InsertPrisonerController.this.getPrisoners(),
                    InsertPrisonerController.this.getCurrentPrisoners(),
                    InsertPrisonerController.this.createPrisoner());
		}
		
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertPrisonerView.dispose();
			new MainController(new SimpleMainView(insertPrisonerView.getRank()));
		}
		
	}
	
	/**
	 * listener che aggiunge un crimine alla lista dei crimini del prigioniero
	 */
	public class AddCrimeListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			//recupero i crimini inseriti nella view
			List<String> crimesList = insertPrisonerView.getList();
			//controllo che il crimine inserito non fosse gia presente
			if(crimesList.contains(insertPrisonerView.getCombo())){
				insertPrisonerView.displayMessage("Crimine già inserito");
			}
			else{
				//inserisco il crimine
				crimesList.add(insertPrisonerView.getCombo());
				insertPrisonerView.setList(crimesList);
			}
		}
		
	}
	
	private boolean isSomethingEmpty(Prisoner p){
		return p.getName().isEmpty() || p.getSurname().isEmpty() || p.getCrimes().size() == 1;
	}

	private boolean areNotDatesCorrect(Date imprisonmentStartDate, Date imprisonmentEndDate,
                                       Date birthDate, Calendar today){
		return imprisonmentStartDate.after(imprisonmentEndDate)||
				imprisonmentStartDate.before(today.getTime())||
				birthDate.after(today.getTime());
	}
	
}
