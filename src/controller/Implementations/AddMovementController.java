package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

import controller.templates.BaseController;
import model.Implementations.SimpleMovement;
import model.builders.SimpleMovementBuilder;
import model.Interfaces.Movement;
import view.views.implementations.SimpleMoreFunctionsView;
import view.views.interfaces.AddMovementView;

/**
 * controller che gestisce la addMovementView
 */
public class AddMovementController extends BaseController<Movement> {

    private static final String MOVEMENTS_FILE_PATH = getResourcesFolderPath() + "AllMovements.txt";
    private static AddMovementView addMovementView;
	
	/**
	 * costruttore 
	 * @param addMovementView la view
	 */
	public AddMovementController(AddMovementView addMovementView){
		AddMovementController.addMovementView=addMovementView;
		AddMovementController.addMovementView.addBackListener(new BackListener());
		AddMovementController.addMovementView.addInsertListener(new InsertListener());
	}
	
	/** torna alla view precedente */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			addMovementView.dispose();
			new MoreFunctionsController(new SimpleMoreFunctionsView(addMovementView.getRank()));
		}
		
	}
	
	/**
	 * listener che inserisce un movimento
	 */
	public class InsertListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			//prendo il movimento dalla view
			SimpleMovement m = createSimpleMovement();
			//se l'amount è inferiore a zero stampo l'errore
			if(m.getAmount() <= 0){
				addMovementView.displayMessage("Input invalido");
				return;
			}
			// recupero tutti i movimenti del passato
			List<Movement> movements = getMovements();
			// aggiungo il nuovo movimento
			movements.add(m);
			// salvo la lista aggiornata
			try {
				setMovements(movements);
			} catch (IOException exception) {
				addMovementView.displayMessage(exception.getMessage());
			}
			addMovementView.displayMessage("Movimento inserito");
		}

	}

	private SimpleMovement createSimpleMovement() {
		return new SimpleMovementBuilder()
                        .setDescription(addMovementView.getDescription())
                        .setAmount(addMovementView.getActionSigns())
                        .setSign(addMovementView.getSymbol().charAt(0))
                        .createSimpleMovement();
	}

	/**
     * salva la lista aggiornata di movimenti
     * @param list lista dei movimenti
     * @throws IOException
     */
    private void setMovements(List<Movement> list) throws IOException{
        saveObjectsFromScratch(list, new File(MOVEMENTS_FILE_PATH));
    }

	/**
	 * ritorna la lista dei movimenti di bilancio
	 * @return lista dei movimenti
	 */
    private List<Movement> getMovements(){
		//recupero il file dove sono salvati i movimenti
		return readObjectsFromFile(new File(MOVEMENTS_FILE_PATH));
	}
	
}
	
	
	

