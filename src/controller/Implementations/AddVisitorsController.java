package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import controller.templates.VisitorsController;
import model.Interfaces.Prisoner;
import model.Interfaces.Visitor;
import view.views.implementations.SimpleMoreFunctionsView;
import view.views.interfaces.AddVisitorsView;


/**
 * controller della addVisitorsView
 */
public class AddVisitorsController extends VisitorsController {

    private static AddVisitorsView visitorsView;

	/**
	 * costruttore
	 * @param view la view
	 */
	public AddVisitorsController(AddVisitorsView view)
	{
		AddVisitorsController.visitorsView=view;
		visitorsView.addBackListener(new BackListener());
		visitorsView.addInsertVisitorListener(new InsertListener());
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			visitorsView.dispose();
			new MoreFunctionsController(new SimpleMoreFunctionsView(visitorsView.getRank()));
		}
		
	}
	
	/**
	 * listener che gestisce l'inserimento di visitatori
	 */
	public class InsertListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			//recupero la lista dei visitatori e la salvo in una lista
			List<Visitor> visitors = getVisitors();
			//salvo il visitatore inserito nella view
			Visitor vi = visitorsView.getVisitor();
			//controllo che non ci siano errori
			if (areVisitorDataCorrect(vi)) {
				//inserisco il visitatore nella lista
                visitors.add(vi);
                visitorsView.displayMessage("Visitatore inserito");
			} else {
				visitorsView.displayMessage("Devi inserire un nome," +
                        " un cognome e un prigioniero esistente");
			}
			//salvo la lista aggiornata
            setVisitors(visitors, visitorsView);
		}

		private boolean areVisitorDataCorrect(Visitor vi) {
			return vi.getName().length() >= 2 && vi.getSurname().length() >= 2
					&& isPrisonerIdPresent(vi);
		}


		/**
         * controlla se l'id del prigioniero inserita è corretta
         * @param visitor visitatore
         * @return true se l'id è corretto
         * @throws ClassNotFoundException
         * @throws IOException
         */
        private boolean isPrisonerIdPresent(Visitor visitor) {
            List<Prisoner> prisonerList = getPrisoners();
            return prisonerList.stream()
                    .anyMatch(prisoner -> prisoner.getPrisonerID() == visitor.getPrisonerID());
        }
	
	}
}



