package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JTable;

import controller.templates.PrisonerController;
import model.Interfaces.Prisoner;
import view.views.implementations.SimpleSupervisorFunctionsView;
import view.views.interfaces.ShowPrisonersView;

/**
 * controller che gestisce la show prisoners view
 */
public class ShowPrisonersController extends PrisonerController {

    private static ShowPrisonersView showPrisonersView;
    private static final String[] PRISONER_FIELD = new String[]{"id", "nome", "cognome", "giorno di nascita", "inizio prigionia", "fine prigionia"};

    /**
	 * controlle 
	 * @param showPrisonersView la view
	 */
	public ShowPrisonersController(ShowPrisonersView showPrisonersView){
		ShowPrisonersController.showPrisonersView=showPrisonersView;
		ShowPrisonersController.showPrisonersView.addBackListener(new BackListener());
		ShowPrisonersController.showPrisonersView.addComputeListener(new ComputeListener());
	}

	/**
	 * listener che apre la view precedente
	 */
	public static class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			showPrisonersView.dispose();
			new SupervisorController(new SimpleSupervisorFunctionsView(showPrisonersView.getRank()));
		}
		
	}
	
	/**
	 * crea una tabella contenente i prigionieri che tra le due date prese in input erano in prigione
	 */
	public class ComputeListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			//salvo le date inserite nella view
			Date from=showPrisonersView.getFrom();
			Date to=showPrisonersView.getTo();
			//recupero la lista dei prigionieri
            List<Prisoner> prisonerList = getPrisoners();
            List<Prisoner> includedPrisoners = getIncludedPrisoners(from, to, prisonerList);
			//creo una matrice con i prigionieri includedPrisoners
            String[][] mat = buildPrisonersMatrix(includedPrisoners);
			//creo la tabella passandogli i dati della matrice
            JTable table=new JTable(mat, PRISONER_FIELD);
			showPrisonersView.createTable(table);
		}
		
	}

    private List<Prisoner> getIncludedPrisoners(Date from, Date to, List<Prisoner> prisonerList) {
        List<Prisoner> includedPrisoners = new ArrayList<>();
        for(Prisoner p : prisonerList){
            //se il prigioniero è imprigionato tra le due date lo aggiungo alla lista includedPrisoners
            if(p.getImprisonmentStartDate().before(to) &&
                    p.getImprisonmentEndDate().after(from)){
                includedPrisoners.add(p);
            }
        }
        return includedPrisoners;
    }

    private String[][] buildPrisonersMatrix(List<Prisoner> inclusi) {
		String[][] mat = new String[inclusi.size()][PRISONER_FIELD.length];
		for(int i=0;i<inclusi.size();i++){
            mat[i][0]=String.valueOf(inclusi.get(i).getPrisonerID());
            mat[i][1]=inclusi.get(i).getName();
            mat[i][2]=inclusi.get(i).getSurname();
            mat[i][3]=inclusi.get(i).getBirthDate().toString();
            mat[i][4]=inclusi.get(i).getImprisonmentStartDate().toString();
            mat[i][5]=inclusi.get(i).getImprisonmentEndDate().toString();
        }
		return mat;
	}
}
