package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.views.implementations.SimpleInsertPrisonerView;
import view.views.implementations.SimpleLoginView;
import view.views.implementations.SimpleMoreFunctionsView;
import view.views.implementations.SimpleRemovePrisonerView;
import view.views.implementations.SimpleSupervisorFunctionsView;
import view.views.implementations.SimplePrisonerView;
import view.views.interfaces.MainView;

/**
 * controller dela main view
 */
public class MainController {

	private final MainView mainView;

	/**
	 * costruttore
	 * @param mainView la view
	 */
	public MainController(MainView mainView){
		this.mainView=mainView;
		mainView.addLogoutListener(new LogoutListener());
		mainView.addInsertPrisonerListener(new InsertPrisonerListener());
		mainView.addRemovePrisonerListener(new RemovePrisonerListener());
		mainView.addViewPrisonerListener(new ViewPrisonerListener());
		mainView.addMoreFunctionsListener(new MoreFunctionsListener());
		mainView.addSupervisorListener(new SupervisorListener());
	}
		
	/**
	 * listener che fa tornare alla login view
	 */
	public class LogoutListener implements ActionListener{
			
		@Override
		public void actionPerformed(ActionEvent arg0) {
			mainView.dispose();
			new LoginController(new SimpleLoginView());
			 
		}
	}
	
	/**
	 * listener che fa andare alla insert prisoner view
	 */
	public class InsertPrisonerListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			mainView.dispose();
			new InsertPrisonerController(new SimpleInsertPrisonerView(mainView.getRank()));
		}
	}
	
	/**
	 * listener che fa andare alla remove prisoner view
	 */
	public class RemovePrisonerListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			 mainView.dispose();
			 new RemovePrisonerController(new SimpleRemovePrisonerView(mainView.getRank()));
		}
	}
	
	/**
	 * listener che fa andare alla view prisoner view
	 */
	public class ViewPrisonerListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			 mainView.dispose();
			 new ViewPrisonerController(new SimplePrisonerView(mainView.getRank()));
		}
	}


	/**
	 * listener che fa andare alla more functions view
	 */
	public class MoreFunctionsListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			 mainView.dispose();
			 new MoreFunctionsController(new SimpleMoreFunctionsView(mainView.getRank()));
		}
	}
		
	/**
	 * listener che fa andare alla supervisoner functions view
	 */
	public class SupervisorListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			 mainView.dispose();
			 new SupervisorController(new SimpleSupervisorFunctionsView(mainView.getRank()));
		}
	}

}
