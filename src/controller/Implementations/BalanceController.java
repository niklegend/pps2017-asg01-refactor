package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;

import javax.swing.JTable;

import controller.templates.BaseController;
import model.Interfaces.Movement;
import view.views.implementations.SimpleMoreFunctionsView;
import view.views.interfaces.BalanceView;

/**
 * controller che gestisce la balance view
 */
public class BalanceController extends BaseController<Movement> {

    private static final String MOVEMENTS_FILE_PATH = getResourcesFolderPath() + "AllMovements.txt";
    private static BalanceView balanceView;

	/**
	 * costruttore
	 * @param balanceView la view
	 */
	public BalanceController(BalanceView balanceView){
		BalanceController.balanceView=balanceView;
		balanceView.addBackListener(new BackListener());
		showBalance();
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			balanceView.dispose();
			new MoreFunctionsController(new SimpleMoreFunctionsView(balanceView.getRank()));
		}
		
	}
	
	private void showBalance(){
		//salvo tutti i movimenti passati in una lista
		List<Movement>list = readObjectsFromFile(new File(MOVEMENTS_FILE_PATH));
        int balance = calculateBalance(list);
		//stampo il bilancio
		balanceView.setLabel(String.valueOf(balance));
		//creo una tabella con tutti i movimenti
        JTable table = buildMovementsTable(list);
		balanceView.createTable(table);
		
	}

    private JTable buildMovementsTable(List<Movement> list) {
        String[]vet={"+ : -","amount","desc","data"};
        String[][]mat=new String[list.size()][vet.length];
        for(int i=0;i<list.size();i++){
                mat[i][0]=String.valueOf(list.get(i).getSign());
                mat[i][1]=String.valueOf(list.get(i).getAmount());
                mat[i][2]=list.get(i).getDescription();
                mat[i][3]= list.get(i).getDate();
        }
        return new JTable(mat,vet);
    }

    private int calculateBalance(List<Movement> list) {
        int balance=0;
        //li ciclo e calcolo il bilancio
        for(Movement m:list){
                switch(m.getSign()){
            case '-' : balance-=m.getAmount();
                break;
            case '+' : balance+=m.getAmount();
                break;
            }
        }
        return balance;
    }
}
