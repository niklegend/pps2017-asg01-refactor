package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Objects;

import javax.swing.JTable;

import controller.templates.VisitorsController;
import model.Interfaces.Visitor;
import view.views.implementations.SimpleMoreFunctionsView;
import view.views.interfaces.VisitorsView;

/**
 * controller della view visitors view
 * @author Utente
 *
 */
public class ViewVisitorsController extends VisitorsController {

	private static final String[] VISITOR_FIELDS = {"Nome", "Cognome", "Data di nascita", "ID prigioniero visitato"};
	private static VisitorsView visitorsView;

	/**
	 * costruttore
	 * @param visitorsView la view
	 */
	public ViewVisitorsController(VisitorsView visitorsView){
		ViewVisitorsController.visitorsView = visitorsView;
		visitorsView.addBackListener(new BackListener());
		visitorsView.createTable(getTable());
	}
	
	/**
	 * listener che apre la view precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			visitorsView.dispose();
			new MoreFunctionsController(new SimpleMoreFunctionsView(visitorsView.getRank()));
		}
		
	}
	
	private JTable getTable(){
		//creo una lista in cui inserisco i visitatori
        List<Visitor> list = getVisitors();
		String[][] mat = buildVisitorsMatrix(list);
		//creo la tabella passandogli la matrice
        return new JTable(mat,VISITOR_FIELDS);
	}

	private String[][] buildVisitorsMatrix(List<Visitor> list) {
		//metto gli elementi della lista in una matrice
		String[][] mat = new String[Objects.requireNonNull(list).size()][VISITOR_FIELDS.length];
		for(int i=0;i<list.size();i++){
			mat[i][0]=list.get(i).getName();
			mat[i][1]=list.get(i).getSurname();
			mat[i][2]=list.get(i).getBirthDate().toString();
			mat[i][3]=String.valueOf(list.get(i).getPrisonerID());
		}
		return mat;
	}

}
