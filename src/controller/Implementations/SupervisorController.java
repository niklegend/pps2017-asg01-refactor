package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.views.implementations.SimpleInsertGuardView;
import view.views.implementations.SimpleMainView;
import view.views.implementations.SimpleRemoveGuardView;
import view.views.implementations.SimpleShowPrisonersView;
import view.views.implementations.SimpleGuardView;
import view.views.interfaces.SupervisorFunctionsView;

/**
 * controller che gestisce la supervisor functions view
 */
public class SupervisorController {

	private static SupervisorFunctionsView supervisorFunctionsView;
	
	/**
	 * costruttore 
	 * @param supervisorFunctionsView la view
	 */
	public SupervisorController(SupervisorFunctionsView supervisorFunctionsView){
		SupervisorController.supervisorFunctionsView=supervisorFunctionsView;
		supervisorFunctionsView.addBackListener(new BackListener());
		supervisorFunctionsView.addShowPrisonersListener(new ShowPrisonersListener());
		supervisorFunctionsView.addInsertGuardListener(new InsertGuardListener());
		supervisorFunctionsView.addRemoveGuardListener(new RemoveGuardListener());
		supervisorFunctionsView.addViewGuardListener(new ViewGuardListener());
	}
	
	/**
	 * listener che apre la view precedente
	 */
	public static class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			supervisorFunctionsView.dispose();
			new MainController(new SimpleMainView(supervisorFunctionsView.getRank()));
		}
		
	}
	
	/**
	 * listener che apre la insert guard view
	 */
	public static class InsertGuardListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			supervisorFunctionsView.dispose();
			new InsertGuardController(new SimpleInsertGuardView(supervisorFunctionsView.getRank()));
		}
		
	}
	
	/**
	 * listener che apre la remove guad view
	 */
	public static class RemoveGuardListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			supervisorFunctionsView.dispose();
			new RemoveGuardController(new SimpleRemoveGuardView(supervisorFunctionsView.getRank()));
		}
		
	}
	
	/**
	 * listener che apre la show prisoners view
	 */
	public static class ShowPrisonersListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			supervisorFunctionsView.dispose();
			new ShowPrisonersController(new SimpleShowPrisonersView(supervisorFunctionsView.getRank()));
		}
		
	}
	
	/**
	 * listener che apre la view guard view
	 */
	public static class ViewGuardListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			supervisorFunctionsView.dispose();
			new ViewGuardController(new SimpleGuardView(supervisorFunctionsView.getRank()));
		}
		
	}
}
