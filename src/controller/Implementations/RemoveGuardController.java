package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import controller.templates.GuardsController;
import model.Interfaces.Guard;
import view.views.implementations.SimpleSupervisorFunctionsView;
import view.views.interfaces.RemoveGuardView;

/**
 * contoller della remove guard view
 */
public class RemoveGuardController extends GuardsController {

    private static final String REMOVED_GUARD_MESSAGE = "Guardia Rimossa";
    private static final String GUARD_NOT_FOUND_MESSAGE = "Guardia non trovata";
    private static RemoveGuardView removeGuardView;
	
	/**
	 * costruttore
	 * @param removeGuardView la view
	 */
	public RemoveGuardController(RemoveGuardView removeGuardView){
		RemoveGuardController.removeGuardView=removeGuardView;
		removeGuardView.addBackListener(new BackListener());
		removeGuardView.addRemoveGuardListener(new RemoveGuardListener());
	}
	
	/**
	 * listener che si occupa della rimozione della guardia
	 */
	public class RemoveGuardListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			removeGuard();
		}

	}
	
	private void removeGuard(){
		//salvo le guardie registrate in una lista
        List<Guard> guards = getGuards();
		if (removeGuardByID(guards)) {
			removeGuardView.displayErrorMessage(REMOVED_GUARD_MESSAGE);
		}else{
            removeGuardView.displayErrorMessage(GUARD_NOT_FOUND_MESSAGE);
        }
        //salvo la lista aggiornata
        updateGuardList(guards);
    }

    private void updateGuardList(List<Guard> guards) {
        try {
            saveGuardList(guards);
        } catch (IOException exception) {
            removeGuardView.displayErrorMessage(exception.getMessage());
        }
    }

    private boolean removeGuardByID(List<Guard> guards) {
		return guards.removeIf(guard -> guard.getID() == removeGuardView.getID());
	}

	/**
	 * listener che apre la view precedente
	 */
	public static class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			removeGuardView.dispose();
			new SupervisorController(new SimpleSupervisorFunctionsView(removeGuardView.getRank()));
		}
		
	}
}
