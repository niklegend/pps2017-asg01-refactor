package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Objects;

import controller.templates.GuardsController;
import model.Interfaces.Guard;
import view.views.implementations.SimpleSupervisorFunctionsView;
import view.views.interfaces.InsertGuardView;

/**
 * controller che gestisce la insert guard view
 */
public class InsertGuardController extends GuardsController {

	private static InsertGuardView insertGuardView;
	
	/**
	 * costruttore
	 * @param insertGuardView la view
	 */
	public InsertGuardController(InsertGuardView insertGuardView){
		InsertGuardController.insertGuardView=insertGuardView;
		insertGuardView.addBackListener(new BackListener());
		insertGuardView.addInsertListener(new InsertListener());
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertGuardView.dispose();
			new SupervisorController(new SimpleSupervisorFunctionsView(insertGuardView.getRank()));
		}
		
	}
	
	private void insertGuard(){
		//salvo le guardie in una lista
		List<Guard> guards = getGuards();
		//recupero la guardia inserita nella view
		Guard g = insertGuardView.getGuard();
        //controllo che non ci siano errori
		if(!insertionContainsErrors(guards, g)){
			//inserisco la guardia e salvo la lista aggiornata
			guards.add(g);
			setGuards(guards);
			insertGuardView.displayErrorMessage("Guardia inserita");

		}
	}

    private boolean insertionContainsErrors(List<Guard> guards, Guard guardToInsert) {
        boolean contains=false;
        for (Guard g1 : Objects.requireNonNull(guards)){
            if(g1.getID()==guardToInsert.getID()){
                insertGuardView.displayErrorMessage("ID già usato");
                contains=true;
            }
        }
        if(isSomethingEmpty(guardToInsert)){
            insertGuardView.displayErrorMessage("Completa tutti i campi correttamente");
            contains=true;
        }
        return contains;
    }

    /**
	 * listener che si occupa di inserire una guardia
	 */
	public class InsertListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertGuard();
		}
		
	}
	
	private boolean isSomethingEmpty(Guard g){
		return g.getName().equals("") || g.getSurname().equals("") ||
				g.getRank() < 1 || g.getRank() > 3 || g.getID() < 0 ||
				g.getPassword().length() < 6;
	}

}
