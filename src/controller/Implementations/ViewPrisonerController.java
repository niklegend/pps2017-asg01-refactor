package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import controller.templates.PrisonerController;
import model.Interfaces.Prisoner;
import view.views.implementations.SimpleMainView;
import view.views.interfaces.PrisonerView;

/**
 * controller della view prisoner view
 */
public class ViewPrisonerController extends PrisonerController {

	private static PrisonerView prisonerView;
	
	/**
	 * costruttore
	 * @param prisonerView la view
	 */
	public ViewPrisonerController(PrisonerView prisonerView){
		ViewPrisonerController.prisonerView = prisonerView;
		prisonerView.addViewListener(new ViewProfileListener());
		prisonerView.addBackListener(new BackListener());
	}
	
	/**
	 * listener che si occupa di far mostrare il prigioniero
	 */
	public class ViewProfileListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			showPrisoner();
		}
		
	}

	private void showPrisoner(){
		if(String.valueOf(prisonerView.getID()).isEmpty()){
			prisonerView.displayErrorMessage("Devi inserire un ID");
		}
		List<Prisoner> prisoners = getPrisoners();
        if(!searchPrisoner(prisoners)){
				prisonerView.displayErrorMessage("Prigioniero non trovato");
		 }
	}

    private boolean searchPrisoner(List<Prisoner> prisoners) {
	    boolean found = false;
        //ciclo tutti i prigionieri
        for(Prisoner p : prisoners){
            if(p.getPrisonerID()== prisonerView.getID()){
                showPrisonerData(p);
                found=true;
            }
        }
        return found;
    }

    private void showPrisonerData(Prisoner p) {
        //quando l'id corrisponde stampo i dati del prigioniero
        prisonerView.setProfile(p.getName(), p.getSurname(),
                p.getBirthDate().toString(),
                p.getImprisonmentStartDate().toString(),
                p.getImprisonmentEndDate().toString());
        prisonerView.setCrimesTextArea(p.getCrimes());
    }

    /**
	 * listener che apre la view precedente
	 */
	public static class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			prisonerView.dispose();
			new MainController(new SimpleMainView(prisonerView.getRank()));
		}
		
	}
}
