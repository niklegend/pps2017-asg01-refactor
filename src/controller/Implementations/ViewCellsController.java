package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JTable;

import controller.templates.CellsController;
import model.Interfaces.Cell;
import view.views.implementations.SimpleMoreFunctionsView;
import view.views.interfaces.CellsView;


/**
 * controller della view cells view
 */
public class ViewCellsController extends CellsController {

    private static CellsView cellsView;
    private static final String[] CELL_FIELDS = new String[]{"ID Cella", "Descrizione", "Prigionieri correnti", "Capacità max"};

    /**
	 * costruttore
	 * @param cellsView la view
	 */
	public ViewCellsController(CellsView cellsView){
		ViewCellsController.cellsView = cellsView;
		cellsView.addBackListener(new BackListener());
		cellsView.createTable(getTable());
	}
	
	/**
	 * listener che apre la view precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			cellsView.dispose();
			new MoreFunctionsController(new SimpleMoreFunctionsView(cellsView.getRank()));
		}

	}

	private JTable getTable(){
		List<Cell> list = getCells();
        String[][] mat = buildCellsMatrix(list);
		//creo la tabella passandogli i dati della matrice
		return buildCellsTable(mat);
	}

    private JTable buildCellsTable(String[][] mat) {
        JTable table=new JTable(mat, CELL_FIELDS);
        table.getColumnModel().getColumn(0).setPreferredWidth(20);
        table.getColumnModel().getColumn(1).setPreferredWidth(200);
        return table;
    }

    private String[][] buildCellsMatrix(List<Cell> list) {
		//salvo i dati delle celle in una matrice
		String[][]mat=new String[list.size()][CELL_FIELDS.length];
		for(int i=0;i<list.size();i++){
			mat[i][0]=String.valueOf(list.get(i).getId());
			mat[i][1]=list.get(i).getPosition();
			mat[i][2]=String.valueOf(list.get(i).getCurrentPrisoners());
			mat[i][3]=String.valueOf(list.get(i).getCapacity());
		}
		return mat;
	}
}
