package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;
import java.util.Objects;

import controller.templates.BaseController;
import model.Interfaces.Guard;
import view.views.implementations.SimpleSupervisorFunctionsView;
import view.views.interfaces.GuardView;

/**
 * controller che gestisce la view guard view
 */
public class ViewGuardController extends BaseController<Guard> {

	private static final String GUARDS_FILE_PATH = "";
	private static GuardView guardView;
	
	/**
	 * costruttore
	 * @param guardView la view
	 */
	public ViewGuardController(GuardView guardView){
		ViewGuardController.guardView = guardView;
		guardView.addBackListener(new BackListener());
		guardView.addViewListener(new ViewGuardListener());
	}
	
	/**
	 * imposta la view in modo da mostrare la guardia
	 */
	public class ViewGuardListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			List<Guard>list;
				list=readObjectsFromFile(new File(GUARDS_FILE_PATH));
			viewGuards(Objects.requireNonNull(list));
		}
		
	}
	
	/**
	 * mostra le caratteristiche della guardia nella gui
	 * @param list lista delle guardie
	 *
	 */
	private static void viewGuards(List<Guard> list){
		boolean isGuardFound = false;
		for(Guard g : list){
			if(g.getID() == guardView.getID()){
				guardView.setName(g.getName());
				guardView.setSurnameLabel(g.getSurname());
				guardView.setBirth(g.getBirthDate().toString());
				guardView.setRank(String.valueOf(g.getRank()));
				guardView.setTelephoneLabel(g.getTelephoneNumber());
				isGuardFound =true;
			}
		}
		if(!isGuardFound)
			guardView.displayErrorMessage("Guardia non trovata");
	}
	
	/**
	 * listener che apre la view precedente
	 */
	public static class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			guardView.dispose();
			new SupervisorController(new SimpleSupervisorFunctionsView(guardView.getRank()));
		}
		
	}
}
