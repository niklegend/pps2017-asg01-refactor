package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import controller.templates.PrisonerController;
import model.Interfaces.Cell;
import model.Interfaces.Prisoner;
import view.views.implementations.SimpleMainView;
import view.views.interfaces.RemovePrisonerView;

/**
 * controller della remove prisoner view
 */
public class RemovePrisonerController extends PrisonerController {

	private static RemovePrisonerView removePrisonerView;
	
	/**
	 * costruttore
	 * @param removePrisonerView la view
	 */
	public RemovePrisonerController(RemovePrisonerView removePrisonerView){
		RemovePrisonerController.removePrisonerView=removePrisonerView;
		removePrisonerView.addRemoveListener(new RemoveListener());
		removePrisonerView.addBackListener(new BackListener());
	}
	
	/**
	 * listener che si occupa di rimuovare il prigioniero
	 */
	public class RemoveListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			removePrisoner();
		}
		
	
	}
	
	private void removePrisoner(){
        //salvo la lista dei prigionieri correnti
		List<Prisoner> currentPrisoners = getCurrentPrisoners();
		//ciclo tutti i prigionieri
        Optional<Prisoner> prisoner = currentPrisoners.stream()
                .filter(p -> p.getPrisonerID()==removePrisonerView.getID())
                .findFirst();
        prisoner.ifPresent(p -> {
                    currentPrisoners.remove(p);
                    removePrisonerView.displayErrorMessage("Prigioniero rimosso");
                    updateCells(p);
                    setCurrentPrisoners(currentPrisoners);
                });
		if (!prisoner.isPresent()){
            removePrisonerView.displayErrorMessage("Prigioniero non trovato");
        }
	}

    private void updateCells(Prisoner p) {
        //recupero la lista di celle
        List<Cell> cellList  = getCells();
        decreasePrisonerCellNumber(p, cellList);
        try {
            //salvo la lista aggiornata di celle
            setCells(cellList);
        } catch (IOException exception) {
            removePrisonerView.displayErrorMessage(exception.getMessage());
        }
    }


    //diminuisco il numero di prigionieri correnti nella cella del prigioniero rimosso
    private void decreasePrisonerCellNumber(Prisoner prisoner, List<Cell> cellList) {
        cellList.stream()
                .filter(c -> prisoner.getCellID()==c.getId())
                .findFirst()
                .ifPresent(c -> c.setCurrentPrisoners(c.getCurrentPrisoners()-1));
    }


    /**
	 * listener che apre la view precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			removePrisonerView.dispose();
			new MainController(new SimpleMainView(removePrisonerView.getRank()));
		}
		
	}
	
	
}