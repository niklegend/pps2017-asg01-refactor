package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import controller.templates.GuardsController;
import model.Interfaces.Guard;
import org.json.simple.parser.ParseException;
import view.views.implementations.SimpleMainView;
import view.views.interfaces.LoginView;

import static controller.templates.CellsController.createCellsFile;

/**
 * controller della login view
 */
public class LoginController extends GuardsController {

    private static final String LOGIN_WELCOME_MESSAGE = "Benvenuto Utente ";
    private final LoginView loginView;

	/**
	 * costruttore
	 * @param loginView la view
	 */
	public LoginController(LoginView loginView){
		this.loginView=loginView;
		loginView.addLoginListener(new LoginListener());
		loginView.displayErrorMessage("accedere con i profili: \n id:3 , password:qwerty \n id:2 , password:asdasd ");
	}

	public void initializeResources() {
	    try {
            createConfigFile();
            createResourcesFolder();
            createGuardsFile();
            createCellsFile();
        }catch(final IOException | ParseException exception){
	        loginView.displayErrorMessage(exception.getMessage());
        }
    }

	/**
	 * listener che si occupa di effettuare il login
	 */
	public class LoginListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			//salvo la lista delle guardie nel file
            List<Guard> guards = getGuards();
			//controllo che non ci siano errori
            if (areCredentialInserted()) {
                if(!login(guards)){
                    loginView.displayErrorMessage("Combinazione username/password non corretta");
                }
            } else {
                loginView.displayErrorMessage("Devi inserire username e password");
            }
        }
	}

    private boolean login(List<Guard> guards) {
        boolean isInside = false;
        for (Guard g : Objects.requireNonNull(guards)){
            if(areCredentialsCorrect(g)){
                isInside = true;
                loginView.displayErrorMessage(LOGIN_WELCOME_MESSAGE + loginView.getUsernameLabel());
                loginView.dispose();
                new MainController(new SimpleMainView(g.getRank()));
            }
        }
        return isInside;
    }

    private boolean areCredentialsCorrect(Guard g) {
        return loginView.getUsernameLabel().equals(String.valueOf(g.getUsername()))
                && loginView.getPasswordLabel().equals(g.getPassword());
    }

    private boolean areCredentialInserted() {
        return !loginView.getUsernameLabel().isEmpty() && !loginView.getPasswordLabel().isEmpty();
    }
}
